<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
//$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');



Route::middleware(['auth'])->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/titles/{id}', 'TitleController@index');
    Route::post('/titles/{id}', 'TitleController@post');


    Route::get('/cart', 'CartController@index')->name('cartindex');
    Route::post('/cart', 'CartController@store')->name('cartstore');
    Route::put('/cart', 'CartController@confirm')->name('cartconfirm');


    Route::get('/tyres/import', 'TyreController@index');
    Route::post('/tyres/import', 'TyreController@post');

    Route::get('/orders', 'OrderController@index')->name('orders');
    Route::get('/ordersadm', 'OrderController@ordersadm')->name('ordersadm');
    Route::post('/ordersadm', 'OrderController@update')->name('ordersupdate');

    Route::get('/products', 'ProductController@index')->name('products');
    Route::get('/productsadm', 'ProductAdmController@index')->name('productsadm');

    Route::get('/branch/products', 'ProductController@branches')->name('branchesproducts');

    Route::get('/branch/groups', 'GroupController@index')->name('groups');
    Route::get('/branch/groups/new', 'GroupController@newForm');
    Route::post('/branch/groups/new', 'GroupController@newFormSubmit');

    Route::get('/branch/groups/{id}', 'GroupController@editForm');
    Route::post('/branch/groups/{id}', 'GroupController@editFormSubmit');
    Route::get('/branch/groups/{id}/delete', 'GroupController@delete');

    Route::get('/branch/exchange', 'ExchangeController@index')->name('exchange');

    Route::get('/sales/import', 'SaleController@importform');
//    Route::post('/sales/import', 'SaleController@importpost');
    Route::post('/sales/import', 'SaleController@importFormSubmit');


    Route::get('/sales/{id}', 'SaleController@myOrgProducts');
    Route::get('/sales', 'SaleController@myOrgProducts')->name('sales');


    Route::get('/branch/containers', 'ContainerController@index')->name('containers');
    Route::get('/branch/containers/new', 'ContainerController@create');
    Route::post('/branch/containers/new', 'ContainerController@createPost');
    Route::get('/branch/containers/{id}', 'ContainerController@order');
    Route::post('/branch/containers/{id}', 'ContainerController@orderPost');


});



Route::middleware(['auth','admin_only'])->group(function () {

    Route::resource('/users', 'UserController');
    Route::resource('/orgs', 'OrgController');
    Route::resource('/margins', 'MarginController');
//    Route::get('/importlog', 'ImportLogController@index');

    Route::get('/admin/topmanager', 'SaleController@topmanager')->name('topmanager');

    Route::get('/admin/login_as/{id}', 'UserController@login_as');

    Route::get('/admin/ref', 'TitleController@ref');
    Route::post('/admin/ref', 'TitleController@refpost');

      // TODO: переделать на resource

    Route::get('/branch/containers/{id}/edit', 'ContainerController@edit');
    Route::post('/branch/containers/{id}/edit', 'ContainerController@editPost');
    Route::delete('/branch/containers/{id}', 'ContainerController@delete');



});




