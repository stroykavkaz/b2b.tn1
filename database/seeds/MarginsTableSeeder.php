<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MarginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('margins')->insert([
        [
        'created_at' => Carbon::now(),
        'title' => 'Дилер',
        'margin' => '0.75',
        ],

        [
        'created_at' => Carbon::now(),
        'title' => 'Дилер VIP',
        'price' => '0.7',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'Филиал',
        'price' => '0.7',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'Розница',
        'price' => '1.4',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'Розница VIP',
        'price' => '1.2',
        ],
      ]);
    }
}
