<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
  			[
  			'created_at' => Carbon::now(),
        'id' => 1,
        'org_id' => 1,
  			'name' => 'admin',
  			'email' => 'admin@tn1.ru',
  			'password' => Hash::make('admin23'),
        'margin_id'=>'1',
  			],
  			[ 'created_at' => Carbon::now(),
        'org_id' => 1,
        'id' => 2,
        'name' => 'Николай Степанов',
  			'email' => 'spb3@tn1.ru',
  	    'password' =>Hash::make('stepan23'),
        'margin_id'=>'1',
  			],

  			[ 'created_at' => Carbon::now(),
        'org_id' => 2,
        'id' => 3,
        'name' => 'Алексей Федотов',
  			'email' => 'alex@tn1.ru',
  	    'password' =>Hash::make('alex23'),
        'margin_id'=>'2',
  			],

  			[ 'created_at' => Carbon::now(),
        'org_id' => 3,
        'id' => 4,
        'name' => 'Семен Николаев',
  			'email' => 'alex@tn1.ru',
  	    'password' =>Hash::make('semen23'),
        'margin_id'=>'3',
  			],
  		]);
    }
}
