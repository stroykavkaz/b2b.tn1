<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('products')->insert([
        [
        'created_at' => Carbon::now(),
        'title' => 'Шина 10-16.5 10PR LQ308 TL SUPERHAWK',
        'price' => '60',
        'category_id' => '1',
        ],

        [
        'created_at' => Carbon::now(),
        'title' => 'Шина 10-16.5 12PR SSR101 TL SUPERHAWK',
        'price' => '56',
        'category_id' => '1',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'Шина 12.5/80-18 12PR LQ105 TL SUPERHAWK',
        'price' => '37',
        'category_id' => '2',
        ],

        [
        'created_at' => Carbon::now(),
        'title' => 'TYRE 14-17.5 14PR LQ308 TL SUPERHAWK',
        'price' => '56',
        'category_id' => '2',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'SET 7.00-12 12PR EF108 TT EXMILE',
        'price' => '37',
        'category_id' => '3',
        ],
        [
        'created_at' => Carbon::now(),
        'title' => 'SET 8.25-15 14PR  LQ301 TT SUPERHAWK',
        'price' => '37',
        'category_id' => '3',
        ],

      ]);
    }
}
