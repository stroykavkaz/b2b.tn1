<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DollarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('dollars')->insert([
        [
        'created_at' => Carbon::now(),
        'rub_rate' => '61.4',
        ],
        [
        'created_at' => Carbon::yesterday(),
        'rub_rate' => '58.8',
        ],
        [
        'created_at' => Carbon::now()->subDay(),
        'rub_rate' => '56.4',
        ],
      ]);
    }
}
