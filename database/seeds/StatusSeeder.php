<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('statuses')->insert([
        [
        'status' => 'Создана',
        'id' => 1,
        ],
        [
        'status' => 'На рассмотрении',
        'id' => 2,
        ],
        [
        'status' => 'Резерв',
        'id' => 3,
        ],
        [
        'status' => 'Выполнено',
        'id' => 4,
        ],
        [
        'status' => 'Отмена',
        'id' => 5,
        ],
        [
        'status' => 'Пауза',
        'id' => 6,
        ],
      ]);
    }
}
