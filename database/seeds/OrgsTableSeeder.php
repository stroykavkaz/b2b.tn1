<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class OrgsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('orgs')->insert([
        [ 'created_at' => Carbon::now(),
        'id'=>1,
        'name' => 'СТРОЙКОМПЛЕКТ-КАВКАЗ',
  			'email' => 'alex@tn1.ru',
  			'phone' => '+7 (812) 331-31-97, +7 (812) 327-01-25',
        'address' => 'пр. Стачки, д.249'
  			],
          [ 'created_at' => Carbon::now(),
              'id'=>2,
              'name' => 'ТАЦИТ',
              'email' => 'spb@tn1.ru',
              'phone' => '+7 (812) 331-31-97, +7 (812) 327-01-25',
              'address' => 'Санкт-Петербург, ул. Парковая, д. 7'
          ],
          [
  			'created_at' => Carbon::now(),
  			'id'=>3,
        'name' => 'ТАЦИТ-М',
  			'email' => 'lara@tn1.ru',
  			'phone' => '+7 (495) 353-54-11, +7 (916) 182-54-44',
        'address' => 'г. Москва, ул. Шоссейная, д.90'
  			],

  		
  		]);
    }
}
