<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('categories')->insert([
      [
      'id' => '1',
      'created_at' => Carbon::now(),
      'category' => 'Шины для спецтехники',
      ],
      [
      'id' => '2',
      'created_at' => Carbon::now(),
      'category' => 'Шины для погрузчиков',
      ],
      [
      'id' => '3',
      'created_at' => Carbon::now(),
      'category' => 'Цельнолитые (гусматик, суперэластик)',
      ],
      [
      'id' => '4',
      'created_at' => Carbon::now(),
      'category' => 'Шины для фронтальных погрузчиков (КГШ)',
      ],
      [
      'id' => '5',
      'created_at' => Carbon::now(),
      'category' => 'Ободные ленты (флиппера, flaps)',
      ],
      [
      'id' => '6',
      'created_at' => Carbon::now(),
      'category' => 'Распродажа',
      ],



      ]);
    }
}
