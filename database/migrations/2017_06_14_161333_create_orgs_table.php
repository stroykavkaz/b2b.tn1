<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('name_short')->nullable();SamSampe text

//            $table->integer('stock_first_cat_row')->nullable();
//            $table->integer('stock_first_attr_col')->nullable();
            $table->integer('stock_price_col')->nullable();
            $table->integer('stock_stock_col')->nullable();


//            $table->integer('sales_first_cat_row')->nullable();
//            $table->integer('sales_first_attr_col')->nullable();
            $table->integer('sales_sales_col')->nullable();


            $table->string('address');
            $table->string('email');
            $table->string('phone');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orgs');
    }
}
