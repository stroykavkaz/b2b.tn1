<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->nullable();
            $table->text('title');
            $table->text('title_key')->nullable();

            $table->float('price')->nullable();

            $table->integer('stock1')->nullable();
            $table->float('price1')->nullable();
            $table->integer('sales1')->nullable();
            $table->integer('stock2')->nullable();
            $table->float('price2')->nullable();
            $table->integer('sales2')->nullable();
            $table->integer('stock3')->nullable();
            $table->float('price3')->nullable();
            $table->integer('sales3')->nullable();

            $table->integer('cont1_1')->nullable();
            $table->integer('cont1_2')->nullable();
            $table->integer('cont1_3')->nullable();


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
