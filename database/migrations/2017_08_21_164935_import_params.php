<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImportParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orgs', function (Blueprint $table) {
            $table->integer('stock_skip_first_rows')->nullable();
            $table->integer('stock_skip_last_rows')->nullable();
            $table->integer('stock_skip_if_empty')->nullable();
            $table->integer('stock_skip_if_notempty')->nullable();
            $table->integer('stock_title_col')->nullable();
            $table->char('stock_charset')->nullable();
            $table->char('sales_charset')->nullable();
            $table->integer('sales_title_col')->nullable();
            $table->integer('sales_skip_first_rows')->nullable();
            $table->integer('sales_skip_last_rows')->nullable();
            $table->integer('sales_skip_if_empty')->nullable();
            $table->integer('sales_skip_if_notempty')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
