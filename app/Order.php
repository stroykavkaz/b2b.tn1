<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = ['info', 'status_id','user_id'];

  public function user()
  {
      return $this->belongsTo('App\User');
  }

  public function status()
  {
      return $this->belongsTo('App\Status');
  }

  public function products()
  {
      return $this->belongsToMany('App\Product')->withPivot('num', 'price');
  }

}
