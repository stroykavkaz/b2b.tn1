<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Margin extends Model
{
	use SoftDeletes;

	protected $guarded = [];

	public function users()
	{
		return $this->hasMany('App\User');
	}

	public function getProcentAttribute()
	{
		if ($this->margin > 1){
			$p = '+'.($this->margin-1)*100;
		 }
			else{
				$p ='-'.(1-$this->margin)*100;
			}
			return $p.'%';
	}
}
