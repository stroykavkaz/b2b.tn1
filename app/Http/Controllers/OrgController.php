<?php

namespace App\Http\Controllers;

use App\Org;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;

class OrgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // \Debugbar::info(Carbon::now());

      $orgs = Org::all();
      return view('orgs/index', ['orgs' => $orgs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   return view('orgs/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
             'name' => 'required|min:5',
         ]);

      $input = $request->all();

       Org::create($input);
       Session::flash('flash_message', "Организация &laquo;$request->name&raquo; создана!");

      return redirect()->route('orgs.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Org  $org
     * @return \Illuminate\Http\Response
     */
    public function show(Org $org)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Org  $org
     * @return \Illuminate\Http\Response
     */
    public function edit(Org $org)
    {


    return view('orgs.edit', ['org'=>$org]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Org  $org
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Org $org)
    {
      $org = Org::findOrFail($org->id);

      $this->validate($request, [
        'name' => 'required',
        'email' => 'required',
        'phone' => 'required',
        ]);

    $input = $request->all();
    $org->fill($input);

     $org->save();

      Session::flash('flash_message', "Редактирование &laquo;$org->name&raquo; произведено успешно");

      return redirect()->route('orgs.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Org  $org
     * @return \Illuminate\Http\Response
     */
    public function destroy(Org $org)
    {
      $org = Org::findOrFail($org->id);

    $org->delete();

    Session::flash('flash_message', "Организация $org->name удалена!");

    return redirect()->route('orgs.index');
    }
}
