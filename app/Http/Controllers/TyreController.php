<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Excel;
use App\ImportLog;
use App\Product;
use App\Category;
use Auth;

class TyreController extends Controller
{
public $tyres = [];
public $categories = [];
public $destinationPath = 'uploads/';

public $uploadedFileName = '';

    public function index()
    {
        return view('tyres.import');

    }


public function post(Request $request)
{
  // заливаем
$this->uploadedFileName = $this->uploadFile($request);

$isParsed = $this->parseExcel($this->destinationPath.$this->uploadedFileName);

$isInserted = $this->insert();

if($isParsed){
   Session::flash('flash_message', "Прайс-лист залит успешно!");
// сохраняем имя залитого файла, и оригинальное имя прайс-листа (от юзера)
$this->log($this->uploadedFileName, $request->plTyres->getClientOriginalName());
} else {
Session::flash('flash_message', "Прайс-лист что-то не залился...");
}
  return view('tyres.import');
}

// вставляем в БД
public function insert()
{
// $product = new Product;
foreach ($this->tyres as $key => $value) {
Product::updateOrCreate(
  ['title'=>  $value['title'],'category_id'=>  $value['category_id']],
  ['price'=>$value['price']]
);
}
foreach ($this->categories as $key => $value) {
  // dd($value);
Category::updateOrCreate(
  ['category'=>  $value['title'], 'id'=> $value['category_id']]
);
// dd($value);
}


}

    /**
     * Import pricelist.
     *
     * @return \Illuminate\Http\Response
     */
public function uploadFile(Request $request)
{
    if ($request->hasFile('plTyres'))
    {
        $file = $request->file('plTyres');
        $extension = $file->getClientOriginalExtension();
        $fileName = date('d-m-y_H-i').'.'.$extension;
        $file = $file->move($this->destinationPath, $fileName);
    }
    return $fileName;
}

public function parseExcel($file)
{
  Excel::load($file, function($reader) {

      $results = $reader->all();

      foreach ($results[1] as $key => $value) {
          $this->categories[$value['category']] = array('title' => $value['category'],'category_id' => (int) $value['category_id'] );
      }
      foreach ($results[0] as $key => $value) {
          $this->tyres[] = array('title' => $value['title'], 'price' => $value['price'], 'category_id' => (int) $this->categories[$value['category']]['category_id']);
      }

      });
return true;     //todo продумать ошибки и эксепшены. при ошибке возвращать false
}

public function log($fname,$original_fname)
{
$i = ImportLog::create(['user_id' => Auth::id(),'fname'=>$fname,'original_fname'=>$original_fname]);

}

}
