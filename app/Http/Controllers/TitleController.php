<?php

namespace App\Http\Controllers;

use App\Title;
use App\Product;
use App\Org;
use Session;
use Illuminate\Http\Request;


class TitleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Request $request)
    {
        $canonical = Product::OrderBy('title')->get();
        $titles = Title::where('org_id',$id)->get();
//dd($titles->toArray());
        $org = Org::find($id);
        return view('titles/index',
            [
                'titles' => $titles,
                'canonical' => $canonical,
                'org' => $org,
                'page' => $request->server('QUERY_STRING')
            ]);
    }

    // записываем соответствия
    public function post(Request $request){

        $parent_ids_count=0; $new_items_count=0;

        foreach($request->parent_for as $title_id => $parent_id){

            if (is_numeric($parent_id)) {  // "ростовского аналог" установлен
                Title::where('id', $title_id)->update(['product_id' => $parent_id]);
                $parent_ids_count++;
            }

            elseif ($parent_id=='NEW'){ // аналога нет, надо создать новый товар
            //создаем новый Product
                $t = Title::find($title_id);

            $inserted_product =  Product::firstOrCreate(
                ['title_key' => $t->title], ['title_key'=>$t->title,'title'=>$t->title]);
            // и устанавливаем его как аналог самому себе
            Title::where('id', $title_id)->update(['product_id' => $inserted_product->id]);
                $new_items_count++;
            }
        }


        Session::flash('flash_message', "Сопоставлено: $parent_ids_count. Добавлено: $new_items_count");

        return redirect()->action('TitleController@index',['id'=>$request->id]);
    }

    public function ref()
    {
        return view('titles/ref',  ['products' => Product::get()]);
    }

    // записываем правильные названия
    public function refpost(Request $request){


        foreach($request->ref as $id => $title){
            Product::find($id)->update(['title' => $title]);
        }
        $count = count($request->ref);
        Session::flash('flash_message', "Обновление $count наименований произведено успешно");
        return redirect()->action('TitleController@ref');
    }


}