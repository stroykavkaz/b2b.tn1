<?php

namespace App\Http\Controllers;

use App\Container;
use App\Product;
use Illuminate\Http\Request;
use Session;
use DB;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $containers = Container::latest()->get();

        return view('containers/index', ['containers' => $containers]);
    }

    public function create()
    {

        return view('containers/create');
   }

    public function createPost(Request $request)
    {
        $container = Container::create($request->all()); // создали контейнер

//        $keys = Product::pluck('id')->all(); // массив со всеми ключами
//        $container->products()->attach($keys); // добавляем все текущие товары, пока "пустые" (т.е. без заказов по филиалам)

        Session::flash('flash_message', "Контейнер &laquo;$container->title&raquo; создан");
        return redirect()->route('containers');
   }

    public function edit($id)
    {
      $container = Container::find($id);
      return view('containers/edit',['container'=>$container]);
    }

    public function editPost(Request $request)
    {

        $container = Container::find($request->id);
        $container->title  = $request->title;
        $container->arrival_at  = $request->arrival_at;
        $container->save();

       Session::flash('flash_message', "Контейнер &laquo;$request->title&raquo; отредактирован");

        return redirect()->route('containers');
    }


    public function order($id)
    {
        $products = Product::OrderBy('title')->get();
        $container = Container::find($id);

        return view('containers/order',['container'=>$container, 'products'=>$products]);
    }

    public function orderPost(Request $request)
    {
//        dd($request->order);
        $container = Container::find($request->id);
        $container->products()->sync($request->order); // проставляем все текущие товары

        Session::flash('flash_message', "Заявка на контейнер &laquo;$container->title&raquo; изменена");

        return redirect()->route('containers');
    }


    public function delete(Request $request)
    {

        $container = Container::find($request->id);
        $container->products()->detach(); // удаление связей из промежуточной таблицы
        Container::destroy($request->id); // удаляем сам контейнер

        Session::flash('flash_message', "Контейнер $container->title удален");
        return redirect()->route('containers');
    }

}
