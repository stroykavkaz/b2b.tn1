<?php

namespace App\Http\Controllers;

use App\Group;
use App\Product;
use Illuminate\Http\Request;
use Session;
use DB;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::all();

        return view('groups/index', ['groups' => $groups]);
    }

    public function newForm()
    {
        $products = Product::OrderBy('title')->get();
        return view('groups/create',['products'=>$products]);
   }

    public function newFormSubmit(Request $request)
    {
//        ddd($request->title);
        $group = Group::create(
            ['title' => $request->title,
                'org_id' => $request->user()->org_id
        ]);

        $group->products()->attach($request->product_ids);

        Session::flash('flash_message', "Группа &laquo;$request->title&raquo; создана");

        return redirect()->route('groups');
   }

    public function editForm($id)
    {
        $products = Product::OrderBy('title')->get();
        $group = Group::find($id);
        $products_selected = $group->products->keyBy('id')->toArray();

//        dd($products_selected);
        return view('groups/edit',
            ['group'=>$group, 'products'=>$products,
            'products_selected'=> $products_selected]);
    }

    public function editFormSubmit(Request $request)
    {

        $group = Group::find($request->id);
        $group->title  = $request->title; // сохраняем название
        $group->save();

//        $group->products()->detach(); //убираем все предыдущие товары
        $group->products()->sync($request->product_ids); // проставляем все текущие товары

        Session::flash('flash_message', "Группа &laquo;$request->title&raquo; сохранена");

        return redirect()->route('groups');
    }

    public function delete(Request $request)
    {
    // удаляем группу и связи
        $title =  Group::find($request->id)->title;
        DB::table('groups')->where('id', '=', $request->id)->delete();
        DB::table('group_product')->where('group_id', '=', $request->id)->delete();

        Session::flash('flash_message', "Группа &laquo;$title&raquo; удалена");

        return redirect()->route('groups');
    }

}
