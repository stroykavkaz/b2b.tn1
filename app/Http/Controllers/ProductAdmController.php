<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductAdmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Product::all();
      $margins = \App\Margin::orderBy('margin')->get();
      $last_usd = \App\Dollar::lastRate();
      //  dd($products->prices);
      return view('productsadm/index', ['products' => $products,'margins' => $margins,'last_usd'=>$last_usd]);
    }


}
