<?php

namespace App\Http\Controllers;

use App\User;
use App\ActionLog;
use Illuminate\Http\Request;
use Session;
use Log;
use Auth;
use Redirect;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      $logs = ActionLog::where('action','login')->take(20)->latest()->get();
     return view('users/index', ['users' => $users,'logs'=>$logs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $orgs = \App\Org::pluck('name','id');
       $margins = \App\Margin::pluck('title','id');
       return view('users/create', ['orgs'=>$orgs,'margins'=>$margins]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request->all());
    $this->validate($request, [
           'name' => 'required|unique:users|min:6',
           'email' => 'required',
           'password' => 'required|min:5',
       ]);

    $input = $request->all();

     User::create($input);
     Session::flash('flash_message', "Пользователь &laquo;$request->name&raquo; создан!");

    return redirect()->route('users.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
       $orgs = \App\Org::pluck('name','id');
       $margins = \App\Margin::pluck('title','id');
      return view('users.edit', ['user'=>$user,'orgs'=>$orgs,'margins'=>$margins]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $user = User::findOrFail($user->id);

      $this->validate($request, [
      'name' => 'required|min:6',
      'email' => 'required',
      ]);

      $input = $request->all();

      $user->disabled_at = ($request->is_disabled) ? date('Y-m-d H:i:s') : NULL;

      $user->fill($input);

      $user->save();

      Session::flash('flash_message', "Редактирование &laquo;$user->name&raquo; произведено успешно");

      return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
      $user = User::findOrFail($user->id);

    $user->delete();

    Session::flash('flash_message', "Пользователь $user->name удален!");

    return redirect()->route('users.index');
    }

    public function login_as($user_id)
    {
        // Login and "remember" the given user...
        Auth::loginUsingId($user_id, true);
        return redirect()->route('home');
    }

}
