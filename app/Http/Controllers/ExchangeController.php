<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{

    public function index()
    {
    $products = Product::all();

    $rnd_spb = $products
        ->where('overageItems1','>',0)
        ->where('overageItems2','<',0)->all();

    $spb_rnd = $products
        ->where('overageItems1','<',0)
        ->where('overageItems2','>',0);



    $rnd_msk = $products
            ->where('overageItems1','>',0)
            ->where('overageItems3','<',0);

    $msk_rnd = $products
            ->where('overageItems1','<',0)
            ->where('overageItems3','>',0);


        $spb_msk = $products
            ->where('overageItems2','>',0)
            ->where('overageItems3','<',0);

        $msk_spb = $products
            ->where('overageItems2','<',0)
            ->where('overageItems3','>',0);





        return view('exchange/index',[
            'rnd_spb'=>$rnd_spb,
            'spb_rnd'=>$spb_rnd,
            'rnd_msk'=>$rnd_msk,
            'msk_rnd'=>$msk_rnd,
            'spb_msk'=>$spb_msk,
            'msk_spb'=>$msk_spb,



        ]);
    }

}
