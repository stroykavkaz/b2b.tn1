<?php

namespace App\Http\Controllers;

use App\ActionLog;
use App\Product;
use App\Title;
use App\Org;
use Carbon\Carbon;
use Excel;
use Session;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    private $upload = 'upload/';
    private $stock_file = '';
    private $sales_file = '';

    private $stock_items = '';
    private $sales_items = '';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function index($org_id = null)
//    {
//        if(!$org_id) $org_id = Auth::user()->org_id;
//        $products = Product::all();
//        $products = $products->sortByDesc('overage_rub');
//        $overage_positive = $products->where('overage_rub', '>', 0)->sum('overage_rub');
//        $overage_negative = $products->where('overage_rub', '<', 0)->sum('overage_rub');
//        $price_sum = $products->sum(function ($i){return $i->my_org_price*$i->my_org_stock;});
//
//        $overage_positive = round($overage_positive,-3);
//        $overage_negative = round($overage_negative,-3);
//        $price_sum = round($price_sum,-3);
//
//        $org_title = Org::find($org_id)->name;
//
//        $titles = Title::where('org_id',$org_id)->get();
//        $my_1c_titles = $titles->keyBy('id');
//        return view('sales/index', [
//            'products' => $products,
//            'overage_positive' => $overage_positive,
//            'overage_negative' => $overage_negative,
//            'price_sum' => $price_sum,
//            'org_title'=>$org_title,
//            'my_1c_titles'=>$my_1c_titles
//        ]);
//    }

    // TODO мой филиал
    public static function cp1252toUTF8(&$value)
    {
        // нет return т.к. передаем по ссылке! http://php.net/manual/ru/function.array-walk-recursive.php#106146
        $value = mb_convert_encoding(mb_convert_encoding($value, "CP1252", "UTF-8"), "UTF-8", "CP1251");
    }


    public function myOrgProducts($org_id = NULL)
    {
        if (!$org_id) $org_id = Auth::user()->org_id;

        $products = Product::all();
        $overage_rub_org = 'overage_rub' . $org_id;

        $products = $products->sortByDesc($overage_rub_org);
        $overage_positive = $products->where($overage_rub_org, '>', 0)->sum($overage_rub_org);
        $overage_negative = $products->where($overage_rub_org, '<', 0)->sum($overage_rub_org);
        $price_sum = $products->sum('total_rub' . $org_id);

        $org_title = Org::find($org_id)->name;

        if ($org_id == 1) { // 1с-ные названия 1 филиала являются ключем, являются "каноническими".
//            $titles = Product::get();
            $titles = DB::table('products')->select('id', 'title_key as title')->get();

            $my_1c_titles = $titles->keyBy('id');
//            dd($my_1c_titles);
        } else { // для прочих филиалов названия
            $titles = Title::where('org_id', $org_id)->get();
            $my_1c_titles = $titles->keyBy('product_id');

        }


        return view('sales/index', [
            'products' => $products,
            'overage_positive' => $overage_positive,
            'overage_negative' => $overage_negative,
            'price_sum' => $price_sum,
            'org_title' => $org_title,
            'my_1c_titles' => $my_1c_titles,
            'org_id' => $org_id
        ]);

    }

    public function topmanager()
    {
        $products = Product::all();
        $total1 = $products->sum('total_rub1');
        $total2 = $products->sum('total_rub2');
        $total3 = $products->sum('total_rub3');

        $overage1_pos = $products->where('overage_rub1', '>', 0)->sum('overage_rub1');
        $overage1_neg = $products->where('overage_rub1', '<', 0)->sum('overage_rub1');

        $overage2_pos = $products->where('overage_rub2', '>', 0)->sum('overage_rub2');
        $overage2_neg = $products->where('overage_rub2', '<', 0)->sum('overage_rub2');

        $overage3_pos = $products->where('overage_rub3', '>', 0)->sum('overage_rub3');
        $overage3_neg = $products->where('overage_rub3', '<', 0)->sum('overage_rub3');


        //todo сейчас в условиях прописано user_id но это конечно не
        //  совсем верно, т.к. у одного филиала может быть несколько
        // юзеров, и они могут независимо обновлять СВОЙ филиал.
        //      пока быстро сделал через юзеров

        $update_stock1 = DB::table('action_logs')->where([['user_id', '=', 1], ['object', '=', 'stock']])->latest()->pluck('created_at')->first();
        $update_stock2 = DB::table('action_logs')->where([['user_id', '=', 3], ['object', '=', 'stock']])->latest()->pluck('created_at')->first();
        $update_stock3 = DB::table('action_logs')->where([['user_id', '=', 4], ['object', '=', 'stock']])->latest()->pluck('created_at')->first();

        $update_sales1 = DB::table('action_logs')->where([['user_id', '=', 1], ['object', '=', 'sales']])->latest()->pluck('created_at')->first();
        $update_sales2 = DB::table('action_logs')->where([['user_id', '=', 3], ['object', '=', 'sales']])->latest()->pluck('created_at')->first();
        $update_sales3 = DB::table('action_logs')->where([['user_id', '=', 4], ['object', '=', 'sales']])->latest()->pluck('created_at')->first();

        $update_stock1 = (new Carbon($update_stock1))->diffForHumans();
        $update_stock2 = (new Carbon($update_stock2))->diffForHumans();
        $update_stock3 = (new Carbon($update_stock3))->diffForHumans();
        $update_sales1 = (new Carbon($update_sales1))->diffForHumans();
        $update_sales2 = (new Carbon($update_sales2))->diffForHumans();
        $update_sales3 = (new Carbon($update_sales3))->diffForHumans();


        $user_logins = ActionLog::where('action', 'login')->take(40)->latest()->get();


        $logs = ActionLog::where('action', 'update')->take(40)->latest()->get();
        $upload_logs = $logs->map(function ($item) {
            if ($item->object == 'stock') $item['object_label'] = 'остатки';
            if ($item->object == 'sales') $item['object_label'] = 'продажи';
            if ($item->action == 'update') $item['action_label'] = 'загружены';

            return $item;
        });


        return view('sales/topmanager', compact(
            ['total1', 'total2', 'total3',
                'overage1_pos', 'overage1_neg',
                'overage2_pos', 'overage2_neg',
                'overage3_pos', 'overage3_neg',
                'update_stock1', 'update_stock2', 'update_stock3',
                'update_sales1', 'update_sales2', 'update_sales3',
                'user_logins', 'upload_logs'
            ]));
    }

    public function importForm($value = '')
    {


        $org = Auth::user()->org; // нам нужны параметры заливки (кодировка)

        return view('sales/import', ['org' => $org]);
    }

    public function importFormSubmit(Request $request)
    {
        $org_cfg = Auth::user()->org;

        if ($request->file('stock')) {
            $this->parseStockFile($request, $org_cfg, $request->input('stock_charset'));

            if ($request->input('debug')) dd($this->stock_items);

            $this->updateStock($this->stock_items, $org_cfg->id);

            Org::where('id', $org_cfg->id)->update(['stock_charset' => $request->input('stock_charset')]);

        }

        if ($request->file('sales')) {
            $this->parseSalesFile($request, $org_cfg, $request->input('sales_charset'));

            if ($request->input('debug')) dd($this->sales_items);

            $this->updateSales($this->sales_items, $org_cfg->id);

            Org::where('id', $org_cfg->id)->update(['sales_charset' => $request->input('stock_charset')]);


        }

        return redirect('sales/import');
    }

    public function parseStockFile($request, $org_cfg, $charset)
    {
        $this->stock_file = $this->uploadFile($request, 'stock');

if(stristr($this->stock_file,',xlsx')) {
    $this->stock_items = $this->readExcel($this->upload . $this->stock_file); // new XLSX reader

}
else {
    $this->stock_items = $this->readfile($this->upload . $this->stock_file); // old XLS reader

}


//        dd($this->stock_items);

        // очистка массива
        $this->stock_items = $this->removeItems(
            $this->stock_items,
            $org_cfg->stock_skip_first_rows,
            $org_cfg->stock_skip_last_rows,
            $org_cfg->stock_skip_if_empty,
            $org_cfg->stock_skip_if_notempty
        );

//        dd($this->stock_items);

        // если надо --перекодируем
        if ($charset == 'cp1252')
            $this->stock_items = $this->changeCharset($this->stock_items, 'cp1252');

//        dd($this->stock_items);

        // оставляем и именуем только нужные столбцы + приведение типа settype()
        $this->stock_items = $this->nameAndTypeFields($this->stock_items,
                ['title' => [$org_cfg->stock_title_col,'string'],
                'price' => [$org_cfg->stock_price_col,'integer'],
                'stock' => [$org_cfg->stock_stock_col,'integer']]
        );


        //"дополняем" стандартными названиями (которые являются ключем при обновлении)
        $this->stock_items = $this->addCanonicalTitle($this->stock_items, $org_cfg->id);
//        dd($this->stock_items);

        if ($org_cfg->id == 1) {
            $this->insertProducts($this->stock_items);
        } else {
            $this->insertTitles($this->stock_items, $org_cfg->id);
        }


    }

    public function uploadFile(Request $request, $filename)
    {
        if ($request->hasFile($filename)) {
            $file = $request->file($filename);
            $extension = $file->getClientOriginalExtension();
            $save_filename = date('d-m-y_H-i-') . $filename . '.' . $extension;
            $file = $file->move($this->upload, $save_filename);
        }

        Session::flash('flash_message3', "Файл &laquo;$file&raquo; сохранен");

        return $save_filename;
    }

    /**
     * Читаем xls xlsx, возвращаем массив
     * новый пакет phpspreadsheet
     */
    public static function readExcel($file)
    {
        if (stristr($file, '.xlsx')) $type = 'Xlsx';
        else $type = 'Xls';

//        https://phpspreadsheet.readthedocs.io/en/develop/topics/accessing-cells/
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($type);
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load($file);

        $worksheet = $spreadsheet->getActiveSheet();
// Get the highest row and column numbers referenced in the worksheet
        $highestRow = $worksheet->getHighestRow(); // e.g. 10
        $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 5

        $arr = [];
        $row_data = [];
        for ($row = 1; $row <= $highestRow; ++$row) {
            for ($col = 1; $col <= $highestColumnIndex; ++$col) {
                $row_data[] = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
            }
            $arr[] = $row_data;
            $row_data = [];
        }

        return collect($arr);
    }

    /**
     * Читаем xls xlsx, возвращаем массив
     * старый пакет PHPExcel
     */
    public static function readfile($file)
    {
        $records = Excel::load($file)->get(); // excel
        $rows = [];
        foreach ($records as $key => $value) {
//            dd($rows[$key]);
            $rows[$key] = $value->toArray();
        }
        $rows = collect($rows);

        return $rows;

    }

    /**
     * @param $collection коллекция
     * @param $remove_top убрать N элементов сверху
     * @param $remove_bottom убрать N элементов снизу
     * @return mixed
     */
    public static function removeItems($collection,
                                       $remove_top,
                                       $remove_bottom = 0,
                                       $remove_if_empty_col = 0,
                                       $stock_skip_if_notempty = 0)
    {

        $collection = $collection->slice($remove_top);
        if ($remove_bottom) $collection = $collection->slice(0, -$remove_bottom);

        // удаляем пустые строки
        if ($remove_if_empty_col) {

            $collection = $collection->filter(function ($value) use ($remove_if_empty_col) {
                return !is_null($value[$remove_if_empty_col]);
            });
        }
        // удаляем непустые строки
        if ($stock_skip_if_notempty) {
            $collection = $collection->filter(function ($value) use ($stock_skip_if_notempty) {
                return is_null($value[$stock_skip_if_notempty]);
            });
        }

        return $collection;
    }

    /**
     * меняем кодировку для каждого элемента коллекции
     * @param Collection $col
     * @param $change_from
     */
    public function changeCharset($col, $change_from)
    {
        if ($change_from == 'cp1252') {
            array_walk_recursive($col, [$this, 'cp1252toUTF8']);

            return $col;

        }
    }

    public function nameAndTypeFields($col, array $cols_names)
    {

        $return = $col->map(function ($item) use ($cols_names) {
            $new = [];

            foreach ($cols_names as $key => $val)
            {  settype($item[$val[0]],$val[1]);
                $new[$key] = $item[$val[0]];}

            return $new;
        });

        return $return;
    }

//    public function colToInt()
//    {
//
//    }

    /** Ищем соответствие для даной фирмы, и добавляем в массив поле title_canonical
     * для главного филиала (org_id=1) просто дублируем title в title_canonical
     * @param array $items
     * @param int $org_id
     */
    private function addCanonicalTitle($col, $org_id)
    {

        return $col->map(function ($item) use ($org_id) {
            if ($org_id == 1) { // Ростову подбор не нужен
                $item['title_key'] = $item['title'];
            } else {
                $item['title_key'] = $this->getCanonicalByTitle($item['title'], $org_id);
            }

            return $item;
        });

    }

    /**
     * поиск главного имени (с помощью таблицы соответствий titles) для позиций филиала
     * @param $title позиция из 1с филиала
     * @return string canonical_title (название в 1с РнД)
     *
     */
    public static function getCanonicalByTitle($title)
    {

        // todo переделать, как-то коряво
        $product_id = Title::where('title', $title)->pluck('product_id')->toArray();

        if (array_key_exists(0, $product_id)) {
            $canonical_title = Product::find($product_id[0]);
            if ($canonical_title) return $canonical_title->title_key;


        }

    }

    public function insertProducts($titles)
    {
        foreach ($titles as $title) {
            Product::firstOrCreate(['title_key' => $title['title_key']], ['title' => $title['title']]);
        }
    }

    public function insertTitles($titles, $org_id)
    {
        foreach ($titles as $title) {
            Title::firstOrCreate(['title' => $title['title'], 'org_id' => $org_id]);
        }

    }


// пополняем справочник соответсвия названий

    /**
     * обновление цен и остатков. обновляем таблицу products (поля price1,price2,stock1,stock2 etc)
     */
    public function updateStock($col, $org_id)
    {
//   перед вставкой обуляем все остатки по данному филиалу, иначе останутся некоторые старые, "неперезалитые" остатки
        DB::table('products')->update(['stock' . $org_id => 0]);

        foreach ($col as $item) {
            Product::where('title_key', $item['title_key'])
             // в файле Мск выведено 2 склада, и надо прибавить (а не перезаписать) остаток второго склада к первому.
            // цену просто обновляем из последнего (не очень логично, но так пока проще)

                ->increment('stock' . $org_id, $item['stock'], ['price' . $org_id => round($item['price'])]);


//                ->update(
//                    ['price' . $org_id => round($item['price']),
//                    'stock' . $org_id => $item['stock']]
//                );
        }

        // лог заливок
        ActionLog::create(['object' => 'stock',
            'action' => 'update',
            'attr1' => $this->stock_file]);
    }

    public function parseSalesFile($request, $org_cfg, $charset)
    {
        $this->sales_file = $this->uploadFile($request, 'sales');

        if(stristr($this->stock_file,',xlsx')) {
            $this->sales_items = $this->readExcel($this->upload . $this->sales_file); // new reader
        }
        else {

        $this->sales_items = $this->readfile($this->upload . $this->sales_file); //old parser

    }


//        dd($this->sales_items);
        // очистка массива
        $this->sales_items = $this->removeItems(
            $this->sales_items,
            $org_cfg->sales_skip_first_rows,
            $org_cfg->sales_skip_last_rows,
            $org_cfg->sales_skip_if_empty,
            $org_cfg->sales_skip_if_notempty
        );


        // если надо --перекодируем
        if ($charset == 'cp1252')
            $this->sales_items = $this->changeCharset($this->sales_items, 'cp1252');

        // оставляем и именуем только нужные столбцы
        $this->sales_items = $this->nameAndTypeFields($this->sales_items,
            ['title' => [$org_cfg->sales_title_col,'string'],
                'sales' => [$org_cfg->sales_sales_col,'integer']]);
//        dd($this->sales_items);
        //"дополняем" стандартными названиями (которые являются ключем при обновлении)
        $this->sales_items = $this->addCanonicalTitle($this->sales_items, $org_cfg->id);

    }

    /**
     * обновление продаж
     */
    public function updateSales($col, $org_id)
    {
        foreach ($col as $item) {
            Product::where('title_key', $item['title_key'])
                ->update(['sales' . $org_id => $item['sales']]);
        }
        ActionLog::create(['object' => 'sales', 'action' => 'update',
            'attr1' => $this->sales_file]);

    }


}
