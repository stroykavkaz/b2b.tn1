<?php

namespace App\Http\Controllers;

use App\Order;
use App\Status;
use Session;
use Illuminate\Http\Request;

class OrderController extends Controller
{
  public function index(Request $request)
  {
      $orders = Order::where('user_id', $request->user()->id)->orderBy('updated_at','desc')->get();
      return view('orders/index', ['orders' => $orders]);
  }
  public function ordersadm(Request $request)
  {
    $statuses = Status::pluck('status','id');
      //  $margins = \App\Margin::pluck('title','id');
      $orders = Order::orderBy('updated_at','desc')->get();
      return view('orders/admin', ['orders' => $orders,'statuses'=>$statuses]);
  }

public function update(Request $request)
{
//   $i = $request->query->all();
// dd($i);
  Order::find($request->order_id)->update(['status_id' => $request->status_id]);

  Session::flash('flash_message', "Статус заявки №$request->order_id изменен!");

 return redirect()->route('ordersadm');

}


}
