<?php

namespace App\Http\Controllers;

use Session;
use App\Cart;
use App\Product;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    // корзина для текущего юзера
     $items = Cart::where('user_id',$request->user()->id)->get();
$json = [];
     foreach ($items as $i) {
      $json[$i->id] = [
        'price' => $i->price,
        'num' => $i->num,
        'title' => $i->product->title,

      ];
     }

    if ($items->isEmpty())
      return view('cart/empty');
      else
      return view('cart/index', ['items' => $json]);

    }


    public function store(Request $request)
    {
      $this->validate($request, [
             'product_id' => 'required|integer',
         ]);

    $product = Product::find($request->product_id);

    $cart = new Cart;
    $cart->product_id = $request->product_id;
    $cart->user_id = $request->user()->id;
    $cart->num = 1;
    $cart->price = $product->myprice;
    $cart->save();

    // количество товаров в корзине
    $cart_count = Cart::where('user_id',$request->user()->id)->count();
      //  Session::flash('flash_message', "Товар &laquo;$product->title&raquo; добавлен!");
      // return redirect()->route('products');
      //
  // TODO: возврат ошибки
  $json['cart_count'] = $cart_count;
  $json['product_title'] = $product->title;

    return response()->json($json);
    }


public function confirm(Request $request)
{
// нажали "Очистить козрину" submit-reset
if($request->reset) {
  $this->destroy($request->user()->id);
  Session::flash('flash_message', "Корзина очищена");
  return redirect()->route('cartindex');
}
// нажали "Отправить заявку" submit-confirm
elseif($request->confirm){
  $this->create_new_order($request); // создаем и заполняем новый заказ
  $this->destroy($request->user()->id);  // очищаем корзину
  return redirect()->route('orders');

  }

}

public function create_new_order(Request $request)
{
//    eval(\Psy\sh());
  // создаем новый заказ
  $create = Order::create(['user_id' => $request->user()->id]);
// d($request->products_id);
  // наполняем OrderProduct прилетевшими позициями (с ценой и количеством)
  foreach ($request->products_id as $cart_id => $num) {
    if ($num) { //если ненулевая позиция
    $item =  Cart::find($cart_id);
    DB::table('order_product')->insert([
      'order_id'=> $create->id,
      'product_id' =>$item->product_id,
      'num' => $num,
      'price' =>$item->price,
    ]);
  }
  }
}


public function update_number(Request $request)
{
foreach ($request->products_id as $id => $num) {
Cart::find($id)->update(['num' => $num]);
}

}

//  очищаем корзину указанного юзера
public function destroy($user_id)
{
DB::table('carts')->where('user_id', '=', $user_id)->delete();
}
}
