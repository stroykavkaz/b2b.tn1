<?php

namespace App\Http\Controllers;

use App\ActionLog;
use Illuminate\Http\Request;

class ActionLogController extends Controller
{

    public static function loggedIn()
    {

        ActionLog::create(['object'=>'user','action'=>'login']);

    }

}
