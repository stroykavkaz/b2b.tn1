<?php

namespace App\Http\Controllers;

use App\Org;
use App\User;
use Illuminate\Http\Request;
use App\Margin;
use Session;

class MarginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $margins = Margin::orderBy('margin')->get();
        return view('margins/index', ['margins' => $margins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('margins/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'title' => 'unique:margins|required|min:5',
         ]);

        $input = $request->all();

        Margin::create($input);
        Session::flash('flash_message', "Наценка &laquo;$request->title&raquo; создана!");

        return redirect()->route('margins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $margin = Margin::findOrFail($id);
        return view('margins.edit', ['margin'=>$margin]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $margin = Margin::findOrFail($id);
        return view('margins.edit', ['margin'=>$margin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $margin = Margin::findOrFail($id);

        $this->validate($request, [
            'title' => 'required|min:5'
        ]);

        $input = $request->all();
        $margin->fill($input);

        $margin->save();

        Session::flash('flash_message', "Редактирование &laquo;$margin->title&raquo; произведено успешно");

        return redirect()->route('margins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $margin = Margin::findOrFail($id);

        $margin->delete();

        Session::flash('flash_message', "Наценка $margin->title удалена!");

        return redirect()->route('margins.index');
    }
}
