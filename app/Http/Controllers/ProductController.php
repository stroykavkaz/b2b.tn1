<?php

namespace App\Http\Controllers;

use App\Product;
use App\Group;
use App\ActionLog;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Product::all();
      return view('products/index', ['products' => $products]);
    }

    public function branches()
    {
        $products = Product::OrderBy('title')->get();
        $groups = Group::OrderBy('title')->get();


        $rnd_updated =  ActionLog::where('user_id',1)->latest()->pluck('created_at')->first()->format('d/m');
        $spb_updated =  ActionLog::where('user_id',2)->latest()->pluck('created_at')->first()->format('d/m');
        $msk_updated =  ActionLog::where('user_id',3)->latest()->pluck('created_at')->first()->format('d/m');
// todo - проверять если запрос вернул пустой результат (т.е. нет даты). сейчас будет ошибка

//        dd($rnd_updated);
         $callback_transfer = function ($item) {
            if ($this->canTransfer(
                $item->overageItems1,
                $item->overageItems2,
                $item->overageItems3
            )) {
                $item['exchange']=TRUE;
            }
        };

//        $callback_transfer2 = function ($item) {
//            $i = $item->products()->get();
//            foreach ($i as $p) {
//
//
//                if ($this->canTransfer($p->overageItems1(),$p->overageItems2(),$p->overageItems3()))
//                {  $p['exchange']=TRUE;
//                d('=true=');}
//            }
//        };
//        $groups->map($callback_transfer2);
       // тут с колбеком не получилось, вычисляю возможный обмен в шаблоне



//        возможен ли обмен? проходим каждую позицию
        $products->map($callback_transfer);

     // TODO фильтруется для всех. когда будут фильтры для каждого филиала -- тут надо это учесть
        $products = $products->filter(function ($value, $key){
           return DB::table('group_product')->where('product_id',$value->id)->get()->isEmpty();
        });
//

        return view('products/branches', ['groups'=>$groups, 'products' => $products,
            'rnd_updated'=>$rnd_updated,
            'spb_updated'=>$spb_updated,
            'msk_updated'=>$msk_updated,
        ]);
    }

    /**
     * возможен ли обмен между филиалами?
     * передаем избыток/недостаток (положительное или отрицательное число)
     * если все числа одного знака -- то FALSE ("менятся" нечем),
     * если есть разные знаки, то TRUE (возможен какой-то обмен)
     * @param array ...$overages
     * @return bool
     */
    public static function canTransfer(...$overages)
    {
      $stack =0;
      // удаляем нулевые $overages
      $overages = array_filter($overages, function($a) { return !empty($a); });

        foreach ($overages as $overage) {
            if ($overage > 0) $stack++; else $stack--;
        }

        if (abs($stack)<count($overages))
            return TRUE;
        else
            return FALSE;

    }

}
