<?php

namespace App\Http\Middleware;

use Closure;

class admin_only
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (\Auth::id()===1) {
            return $next($request);
        } else {
            return redirect('home');
        }
    }
}
