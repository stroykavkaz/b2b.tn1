<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['title','org_id'];

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

}
