<?php

namespace App\Listeners;

use App\Events\Event;
use App\Http\Controllers\ActionLogController;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle($event)
    {
//        dd($event->user->name);
        ActionLogController::loggedIn(); //user_id записывается автоматом
    }
}
