<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dollar extends Model
{
  /**
 * The attributes that should be mutated to dates.
 *
 * @var array
 */
protected $dates = [
    'created_at',
    'updated_at',
];

    //
    //
  public static function lastRate()
  {
  $last_rate = DB::table('dollars')->latest()->first();
    return $last_rate;
  }
}
