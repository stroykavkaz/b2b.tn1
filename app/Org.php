<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Org extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'phone', 'address','email'
    ];

    /**
 * The attributes that should be mutated to dates.
 *
 * @var array
 */
protected $dates = ['deleted_at','disabled_at'];

public function users()
    {
        return $this->hasMany('User');
    }

}
