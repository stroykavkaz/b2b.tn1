<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionLog extends Model
{

    protected $fillable = [
        'object', 'action', 'text','attr1','attr2','attr3'
    ];



    public function __construct(array $attributes = [])
    {

        $this->ip_address = \Request::ip();
        $this->user_id = \Auth::user()->id;
        $this->useragent = \Request::header('User-Agent');

        parent::__construct($attributes);
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }



}
