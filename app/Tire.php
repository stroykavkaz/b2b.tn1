<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Tire extends Model
{
	use SoftDeletes;
private $import_months = 3; // за сколько месяцев сделан анализ продаж из 1С (в файле импорта)
private $reserve_months = 2; // на сколько месяцев нужен резерв

protected $fillable = ['title_1c','category_1c','price','stock','sales'];

public function getAvgAttribute($value='') // средние продажи за 1мес
{
	$return = $this->sales /  $this->import_months;
	return $return;
}

public function getReserveAttribute($value='') // необходимый резерв, на 2 мес
{
	$return = $this->avg * $this->reserve_months;
	return $return;
}

public function getOverageItemsAttribute($value='') // избыток на складе, шт
{
	// остатки минус резерв
	$return = $this->stock - $this->reserve;
	return $return;
}

public function getOverageRubAttribute($value='') // избыток на складе, сумма
{
	// избыток умножить на себестоимость
	$return = $this->overage_items * $this->price;
	return $return;
}


}
