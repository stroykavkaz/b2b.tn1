<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Container extends Model
{
    use SoftDeletes;

    protected $fillable = ['title','arrival_at'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'arrival_at',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('org1', 'org2','org3');;
    }

}
