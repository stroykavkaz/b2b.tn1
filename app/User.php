<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'org_id', 'margin_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
 * The attributes that should be mutated to dates.
 *
 * @var array
 */
protected $dates = ['deleted_at','disabled_at'];


    public function org()
    {
        return $this->belongsTo('App\Org');
    }
    public function margin()
    {
        return $this->belongsTo('App\Margin');
    }
    public function cart()
    {
        return $this->hasOne('App\Cart');
    }

}
