<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Title extends Model
{
  protected $fillable = [
    'title', 'primary_title_id', 'org_id' ];

  public function org()
  {
      return $this->belongsTo('App\Org');
  }

  public function canonical()
  {
      return $this->belongsTo('App\Title','parent_id','id');
  }

}
