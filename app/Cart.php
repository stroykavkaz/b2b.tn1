<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Cart extends Model
{
  protected $fillable = ['num', 'price', 'product_id','user_id'];
  public function user()
  {
      return $this->hasMany('App\User');
  }

  public function product()
  {
      return $this->belongsTo('App\Product');
  }

// public function cart_count()
//   {
//     return Cart::where('user_id', Auth::user()->id)->count();
//   }

}
