<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
	use SoftDeletes;



protected $fillable = ['title','title_key','price','category_id',
    'stock1','stock2','stock3',
    'price1','price2','price3',
    'sales1','sales2','sales3'
    ];

    private $import_months = 3; // за сколько месяцев сделан анализ продаж из 1С (в файле импорта)
    private $reserve_months = 2; // на сколько месяцев нужен резерв

    ///////////////////////////

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }


  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function orders()
  {
    return $this->belongsToMany('App\Order');
  }

  public function containers()
    {
        return $this->belongsToMany('App\Container')->withPivot('org1', 'org2','org3');
    }
///////////////////////



    public function getAvgAttribute($value='') // средние продажи за 1мес
    {
        $return = $this->my_org_sales /  $this->import_months;
        $return = round($return);
        return $return;
    }

//    public function getAvg1Attribute($value='') // средние продажи за 1мес РОСТОВ
//    {
//        $return = $this->sales1 /  $this->import_months;
//        $return = round($return);
//        return $return;
//    }

    public function avg1() // средние продажи за 1мес
    {
        $return = $this->sales1 /  $this->import_months;
        $return = round($return);
        return $return;
    }

    public function avg2() // средние продажи за 1мес СПб
    {
        $return = $this->sales2 /  $this->import_months;
        $return = round($return);
        return $return;
    }

    public function avg3() // средние продажи за 1мес Мск
    {
        $return = $this->sales3 /  $this->import_months;
        $return = round($return);
        return $return;
    }

////////////// необходимый резерв, на 2 мес

    public function getReserveAttribute($value='') // необходимый резерв, на 2 мес
    {
        $return = $this->avg * $this->reserve_months;
        $return = round($return);
        return $return;
    }

    public function reserve1() //  РОСТОВ
    {
        $return = $this->avg1() * $this->reserve_months;
        $return = round($return);
        return $return;
    }

    public function reserve2() //  СПб
    {
        $return = $this->avg2() * $this->reserve_months;
        $return = round($return);
        return $return;
    }

    public function reserve3() //  Мск
    {
        $return = $this->avg3() * $this->reserve_months;
        $return = round($return);
        return $return;
    }

//////////избыток на складе, шт
    public function getOverageItemsAttribute($value='') // избыток на складе, шт
    {
        // остатки минус резерв
        $return = $this->my_org_stock - $this->reserve;
        $return = round($return);
        return $return;
    }

    public function getOverageItems1Attribute($value='') // избыток на складе, шт
    {
        // остатки минус резерв
        $return = $this->stock1 - $this->reserve1();
        $return = round($return);
        return $return;
    }

    public function getOverageItems2Attribute($value='') // избыток на складе, шт
    {
        // остатки минус резерв
        $return = $this->stock2 - $this->reserve2();
        $return = round($return);
        return $return;
    }

    public function getOverageItems3Attribute($value='') // избыток на складе, шт
    {
        // остатки минус резерв
        $return = $this->stock3 - $this->reserve3();
        $return = round($return);
        return $return;
    }


    public function getOverageRub1Attribute($value='')
    {
        $return = ($this->stock1 - $this->reserve1())*$this->price1;
          return $return;
    }

    public function getOverageRub2Attribute($value='')
    {
        $return = ($this->stock2 - $this->reserve2())*$this->price2;
          return $return;
    }

    public function getOverageRub3Attribute($value='')
    {
        $return = ($this->stock3 - $this->reserve3())*$this->price3;
        return $return;
    }

//    public function overageItems1() // избыток на складе, шт РОСТОВ
//    {
//        // остатки минус резерв
//        $return = $this->stock1 - $this->reserve1();
//        $return = round($return);
//        return $return;
//    }
//
//    public function overageItems2() // избыток на складе, шт СПб
//    {
//        // остатки минус резерв
//        $return = $this->stock2 - $this->reserve2();
//        $return = round($return);
//        return $return;
//    }
//
//    public function overageItems3() // избыток на складе, шт Мск
//    {
//        // остатки минус резерв
//        $return = $this->stock3 - $this->reserve3();
//        $return = round($return);
//        return $return;
//    }
    public function getTotalRub1Attribute()
    {
        return $this->stock1*$this->price1;
    }

    public function getTotalRub2Attribute()
    {
        return $this->stock2*$this->price2;
    }

    public function getTotalRub3Attribute()
    {
        return $this->stock3*$this->price3;
    }



    public function getOverageRubAttribute($value='') // избыток на складе, сумма
    {
        // избыток умножить на себестоимость
        $return = $this->overage_items * $this->my_org_price;
        $return = round($return);
        return $return;
    }



    public function getMyOrgSalesAttribute()
    {
    $org_id = \Auth::user()->org_id;
    switch ($org_id){
        case 1:
            return $this->sales1;
            break;
        case 2:
            return $this->sales2;
            break;
        case 3:
            return $this->sales3;
            break;
    }
    }

    public function getMyOrgStockAttribute()
    {
        $org_id = \Auth::user()->org_id;
        switch ($org_id){
            case 1:
                return $this->stock1;
                break;
            case 2:
                return $this->stock2;
                break;
            case 3:
                return $this->stock3;
                break;
        }
    }

    public function getMyOrgPriceAttribute()
    {
        $org_id = \Auth::user()->org_id;
        switch ($org_id){
            case 1:
                return $this->price1;
                break;
            case 2:
                return $this->price2;
                break;
            case 3:
                return $this->price3;
                break;
        }
    }

public function getOrdersumAttribute($value='')
{
	$return = $this->pivot->num * $this->pivot->price;
	return $return;
}



	// массив, цены для всех наценок
	public function getPricesAttribute($value='')
	{
	$margins = DB::table('margins')->pluck('margin', 'id')->all();

		foreach ($margins	as $key => $margin) {
		$usd = $this->price * $margin; // цена для каждого уровня
		$r[$key] = round($usd * \App\Dollar::lastRate()->rub_rate,-2); //умножаем на свежий курс, округляем
		}
		return $r;
	}

	// массив, персональная цена, с учетом скидки/наценки текущего юзера
	public function getMyPriceAttribute()
	{

		$usd = $this->price * \Auth::user()->margin->margin; // цена для текущего юзера
		$r = round($usd * \App\Dollar::lastRate()->rub_rate, -2); //умножаем на свежий курс, округляем

		return $r;
	}




}
