<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportLog extends Model
{
	protected $guarded = [];
	protected $fillable = ['user_id','fname','original_fname'];

	public function user(){
		return $this->belongsTo('App\User');
	}
}
