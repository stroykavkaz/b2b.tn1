
## Партнерский портал TN1


### Установка

    git clone git@gitlab.com:stroykavkaz/b2b.tn1.git
    composer install
            
    cp .env.example .env # и прописать доступ к БД  
	    
    php artisan key:generate
    php artisan migrate 
       
    php artisan serve # localhost:8000 запуск веб-сервера
