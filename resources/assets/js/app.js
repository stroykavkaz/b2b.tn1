
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('bootstrap-notify');

// var Turbolinks = require("turbolinks");

const Flatpickr = require('flatpickr');
const Russian = require("flatpickr/dist/l10n/ru.js").ru;
Flatpickr.localize(Russian); // default locale is now Russian

window.Vue = require('vue');

// https://chmln.github.io/flatpickr/

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('vuecart', require('./components/cart.vue'));

const app = new Vue({
    el: '#vueapp'
});

// https://github.com/turbolinks/turbolinks
// Turbolinks.start();


$(document).ready( function () {


  $('#sales_table').DataTable({
      "searching": false,
      "info": false,
      "paging": false,
      "order": [[ 8, 'desc']]
      // fixedHeader: true,
  });

    $('#zerostock_table').DataTable({
        "searching": false,
        "info": false,
        "paging": false,
        "order": [[ 5, 'asc']]
        // fixedHeader: true,
    });


    // /branch/products
  $(".branch_table .js-group").click(
      function () {
        var tr_id =  ".gproducts_" + $(this).data('id').toString();
        $(tr_id).toggle(200);
      }

);

    $(".js-datapicker").flatpickr();

} );
