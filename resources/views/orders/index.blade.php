
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

{{-- <h3>Заявки</h3> --}}

  @foreach ($orders as $order)
              <div class="panel panel-default">
                  <div class="panel-heading"><h3 class="panel-title">Заявка №{{$order->id}}</h3>
                    от {{$order->created_at->toDateString() }}
<span class="pull-right label label-status{{$order->status->id}}">{{$order->status->status}}</span>
                  </div>


        {{-- <div class="panel-body">       </div> --}}

        <table class="table table-hover">

      {{-- @dd($order->orderproduct)     --}}
  @foreach ($order->products as $product)
              <tr>
                <td>{{ $product->title }}
                </td>
                <td>{{ $product->pivot->num }} * {{ $product->pivot->price }}&thinsp;&#8381;
              </td>
                <td>{{ $product->ordersum }}&thinsp;&#8381;
                </td>
              </tr>
  @endforeach
  <tfoot><td></td>
    <td>Итого        </td>
    <td><b>{{$order->products->sum('ordersum')}}</b>&thinsp;&#8381;</td>
  </tfoot>
        </table>


      </div>

@endforeach
    </div>
  </div>
</div>
@endsection
