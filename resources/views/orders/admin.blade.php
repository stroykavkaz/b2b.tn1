
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif

<h3>Все заявки</h3>

  @foreach ($orders as $order)
              <div class="panel panel-default">
                  <div class="panel-heading"><b>{{ $order->user->name }}</b> <br>
                    Заявка №{{ $order->id }} от {{ $order->created_at->toDateString() }}
<span class="pull-right label label-status{{$order->status->id}}">{{$order->status->status}}</span>
                  </div>


        {{-- <div class="panel-body">       </div> --}}

        <table class="table table-hover">

      {{-- @dd($order->orderproduct)     --}}
  @foreach ($order->products as $product)
              <tr>
                <td>{{ $product->title }}
                </td>
                <td>{{ $product->pivot->num }} * {{ $product->pivot->price }}&thinsp;&#8381;
              </td>
                <td>{{ $product->ordersum }}&thinsp;&#8381;
                </td>
              </tr>
  @endforeach
  <tfoot><td>
    {!! Form::model($order, [
        'method' => 'POST',
        'route' => ['ordersupdate']
    ]) !!}
    <input type="hidden" name="order_id" value="{{$order->id}}">
       {{ Form::select('status_id', $statuses, $order->status_id,['class' => 'form-control input-sm','style'=>'width:12em;display:inline-block']) }}
       <button class="btn btn-primary btn-xs" type="submit" name="button">Изменить статус</button>
    {{-- {!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!} --}}
    {!! Form::close() !!}


  </td>
    <td>Итого        </td>
    <td><b>{{$order->products->sum('ordersum')}}</b>&thinsp;&#8381;</td>
  </tfoot>
        </table>


      </div>

@endforeach
    </div>
  </div>
</div>
@endsection
