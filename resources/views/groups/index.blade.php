@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Группы (аналоги) товаров</h3></div>


                    <div class="panel-body">
                        <a href="/branch/groups/new" class="btn btn-success btn-sm">Новая группа</a>
                    </div>

                    <table class="table table-hover">

                        @foreach ($groups as $group)
                            <tr>
                    <td>

                        <small>№{{ $group->id }}</small> <a href="/branch/groups/{{ $group->id }}">{{ $group->title }}</a>
                        <br>
                       <ul> <small>@php foreach ($group->products()->get() as $product) { @endphp
                            <li>{{ $product->title }}</li>
                            @php } @endphp</small></ul>
                             <a href="/branch/groups/{{$group->id}}/delete" onclick="return confirm('Действительно удалить &laquo;{{$group->title}}&raquo;?')"  class="btn btn-default btn-xs">Удалить</a>

       </td>
             <td>


             </td>
         </tr>

     @endforeach


 </table>

</div>
</div>
</div>
</div>
@endsection
