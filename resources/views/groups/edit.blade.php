
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Редактировать &laquo;{{$group->title}}&raquo;</h3></div>

                <div class="panel-body">

{!! Form::open(array('url' => '/branch/groups/'.$group->id,'method'=>'POST')) !!}
<div class="form-group">
  <strong>Название</strong>
  {!! Form::text('title', $group->title, array('placeholder' => 'Название','class' => 'form-control','required' => 'required')) !!}
 </div>

<div class="form-group">
    <strong>Выберите несколько товаров</strong> <br>
{{--{{s($products_selected) }}--}}
        @foreach($products as $key => $product)

               <label style="font-weight: normal;">
                   <input name="product_ids[]" type="checkbox" value="{{$product->id}}" @if (array_key_exists($product->id,$products_selected)) checked @endif> {{$product->title}}</label><br>

        @endforeach
      </div>


{!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
