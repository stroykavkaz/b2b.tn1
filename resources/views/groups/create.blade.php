
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Создать новую группу</h3></div>

                <div class="panel-body">

{!! Form::open(array('url' => '/branch/groups/new','method'=>'POST')) !!}
<div class="form-group">

  {!! Form::text('title', '', array('placeholder' => 'Укажите название группы','class' => 'form-control','required' => 'required')) !!}
 </div>

<div class="form-group">
    <strong>Выберите несколько товаров</strong> <br>

{{--    {{Form::select('sports',$products,null,array('multiple'=>'multiple','name'=>'sports[]'))}}--}}


        @foreach($products as $key => $product)

               <label style="font-weight: normal;"><input name="product_ids[]" type="checkbox" value="{{$product->id}}"> {{$product->title}}</label><br>

        @endforeach
      </div>


{!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
