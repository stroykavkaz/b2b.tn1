@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Контейнеры</h3></div>


                    <div class="panel-body">
                        @if (Auth::id() == 1)
                        <a href="/branch/containers/new" class="btn btn-success btn-sm">Новый контейнер</a>
                        @endif

                    </div>

                    <table class="table table-hover">
                        <thead><tr>
                            <th>Контейнер</th>
                            <th>Дата прихода</th>
                            <th></th>
                        </tr></thead>
                        @foreach ($containers as $container)
                            <tr>

                    <td>
                       <a href="/branch/containers/{{ $container->id }}">{{ $container->title }}</a>
      </td>
                                <td> {{ $container->arrival_at->format('d M y') }} <br><small>{{ $container->arrival_at->diffForHumans() }}</small></td>
       <td>
           @if (Auth::id() == 1)
           <a class="btn btn-default btn-xs"  href="/branch/containers/{{ $container->id }}/edit">Редактировать</a>
           @endif
       </td>

         </tr>

     @endforeach


 </table>

</div>
</div>
</div>
</div>
@endsection
