
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Контейнер {{$container->title}}</h3>
                  {{$container->arrival_at->format('d/m/Y')}}</div>

                <div class="panel-body">

{!! Form::open(array('url' => '/branch/containers/'.$container->id,'method'=>'POST')) !!}

<div class="form-group">

    <table class="table table-condensed table-hover">
        <thead><tr>
            <th></th>
            <th>Ростов</th>
            <th>Санкт-Петербург</th>
            <th>Москва</th>
        </tr></thead>
        @foreach($products as $product)
            @php
                  $col = $product->containers()->get(); // получаем все контейнеры, куда входит этот товар
                   $cont = $col->first(function($val,$key) use ($container) {return $val->id==$container->id;}); // ищем текущий контейнер

               if($cont) { // если вообще контейнер с этими товарами?
              $org1 = $cont->pivot->org1;
              $org2 = $cont->pivot->org2;
              $org3 = $cont->pivot->org3;
               } else {
              $org1 = '';
              $org2 = '';
              $org3 = '';
            }
                    @endphp
<tr>
    <td class="title_width">{{$product->title }}</td>
    @if (Auth::id() == 1)
        <td class="cont_input"><input name="order[{{$product->id}}][org1]" min=0 max=500 value="{{$org1}}" style="width: 4em;" type="number"></td>
        <td class="cont_input"><input name="order[{{$product->id}}][org2]" min=0 max=500 value="{{$org2}}" style="width: 4em;" type="number"></td>
        <td class="cont_input"><input name="order[{{$product->id}}][org3]" min=0 max=500 value="{{$org3}}" style="width: 4em;" type="number"></td>
    @elseif (Auth::user()->org_id == 2)
        <td class="cont_input">{{$org1}}</td>
        <td class="cont_input"><input name="order[{{$product->id}}][org2]" min=0 max=500 value="{{$org2}}" style="width: 4em;" type="number"></td>
        <td class="cont_input">{{$org3}}</td>
    @elseif (Auth::user()->org_id == 3)
            <td class="cont_input">{{$org1}}</td>
            <td class="cont_input">{{$org2}}</td>
            <td class="cont_input"><input name="order[{{$product->id}}][org3]" min=0 max=500 value="{{$org3}}" style="width: 4em;" type="number"></td>
        @else
        <td class="cont_input">{{$org1}}</td>
        <td class="cont_input">{{$org2}}</td>
        <td class="cont_input">{{$org3}}</td>

    @endif



</tr>

@php  $org1=''; $org2=''; $org3=''; @endphp

        @endforeach
    </table>
      </div>


{!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
