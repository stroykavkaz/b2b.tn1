
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Редактировать контейнер &laquo;{{$container->title}}&raquo;</h3></div>

                <div class="panel-body">

{!! Form::open(array('url' => '/branch/containers/'.$container->id.'/edit','method'=>'POST')) !!}
<div class="form-group">
  <strong>Примечание</strong>
  {!! Form::text('title', $container->title, array('placeholder' => 'Примечание','class' => 'form-control','required' => 'required')) !!}
 </div>

<div class="form-group">
<label for="">Дата поступления</label>
<input required value="{{$container->arrival_at->format('Y-m-d')}}" name="arrival_at" class="form-control js-datapicker" type="date" />


</div>
<div class="row">

    <div class="col-md-6 text-left">
        {!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>

    <div class="col-md-6 text-right">
        {!! Form::open([
            'method' => 'DELETE',
            'url' => '/branch/containers/'.$container->id,

        ]) !!}
        <input type="submit" value="Удалить" class="btn btn-danger" onclick="return confirm('Удалить контейнер &laquo;{{ $container->title }}&raquo;?')">

           {!! Form::close() !!}
    </div>

</div>




  </div>
            </div>
        </div>
    </div>
</div>
@endsection
