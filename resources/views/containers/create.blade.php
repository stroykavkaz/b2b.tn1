
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Создать новый контейнер</h3></div>

                <div class="panel-body">

{!! Form::open(array('url' => '/branch/containers/new','method'=>'POST')) !!}
<div class="form-group">

  {!! Form::text('title', '', array('placeholder' => 'Укажите примечание, номер','class' => 'form-control','required' => 'required')) !!}

</div>

<div class="form-group">
<label>Дата поступления</label>
<input required name="arrival_at" class="form-control js-datapicker" type="date" />
</div>


{!! Form::submit('Создать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
