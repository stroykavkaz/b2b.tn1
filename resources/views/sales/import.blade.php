@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @include('_partials.errors')
                @include('_partials.flash_msg')

                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Импорт данных 1С</h3>
                        файлы должны быть в формате Excel
                    </div>
                    <div class="panel-body">
                        {!! Form::open(array('url' => '/sales/import','method'=>'POST', 'files'=>TRUE)) !!}
                        <div class="form-group">
                            <strong>1С Остатки</strong>
                            {!! Form::file('stock') !!}

                            Кодировка
                            <label for="stock1"><input required @if ($org->stock_charset=='cp1252') checked
                                                       @endif type="radio" name="stock_charset" value="cp1252"
                                                       id="stock1">CP1252</label>
                            <label for="stock2"><input required @if ($org->stock_charset=='utf8') checked
                                                       @endif type="radio" name="stock_charset" value="utf8"
                                                       id="stock2">UTF-8</label>

                            {{--<pre>{!! $org->stock_charset !!}</pre>--}}
                        </div>

                        <br>

                        <div class="form-group">
                            <strong>1С Анализ продаж за 3 месяца</strong>
                            {!! Form::file('sales') !!}
                            Кодировка
                            <label for="sales1"><input required required @if ($org->sales_charset=='cp1252') checked
                                                       @endif type="radio" name="sales_charset" value="cp1252"
                                                       id="sales1">CP1252</label>
                            <label for="sales2"><input required required @if ($org->sales_charset=='utf8') checked
                                                       @endif type="radio" name="sales_charset" value="utf8"
                                                       id="sales2">UTF-8</label>

<span onclick="$('#hide').toggle()" style="color: #ccc;float: right;cursor: pointer">+</span>
                            <div id="hide" style="display:none">
                            <label for="debug"><input type="checkbox" name="debug" id="debug">Отладка заливки</label>
</div>

                        </div>


                        {!! Form::submit('Загрузить', ['class' => 'btn btn-primary']) !!}


                        {!! Form::close() !!}


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
