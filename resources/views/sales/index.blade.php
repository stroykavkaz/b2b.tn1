

@extends('layouts.app')
@section('content')

    <style>

        .overage_rub{
            text-align: right!important;
        }
    </style>

<div class="container my-wide-container">
    <div class="row">
        <div class="col-md-12">

          @if(Session::has('flash_message'))
              <div class="alert alert-success">
                  {{ Session::get('flash_message') }}
              </div>
          @endif


            <div class="panel panel-default">

                <div class="panel-heading">
                    <h4>{{$org_title}}</h4>
                    <h4 class="panel-title">Анализ и прогноз продаж </h4>

                </div>

                <div class="panel-body">
             <div style="text-align: right">
                 <small>ИТОГО</small> <br>
                 Себестоимость: {{number_format(round($price_sum,-3),0,'','&thinsp;') }}&#8202;<small class="text-verymuted">&#8381;</small><br>
                 Избыток: {{number_format(round($overage_positive,-3),0,'','&thinsp;') }}&#8202;<small class="text-verymuted">&#8381;</small><br>
                 Недостаток: {{number_format(round($overage_negative,-3),0,'','&thinsp;') }}&#8202;<small class="text-verymuted">&#8381;</small>

</div>

<table id="sales_table" class="table table-hover table-striped table-condensed">
<thead>
  <tr>
    {{-- <th>Категория</th> --}}
    <th>Общее название</th>
    <th>1С <small>{{$org_title}}</small></th>
    <th>***</th>
    <th>Склад,<br>шт</th>
    <th>Продажи <br>за 3мес</th>
    <th>Средн <br>за месяц</th>
    <th>Запас <br>на 2 мес</th>
    <th>Избыток,<br> шт</th>
    <th  data-class-name="overage_rub">Избыток,<br> руб</th>
  </tr>
</thead>


@php
    $price  = 'price'.$org_id;
    $stock ='stock'.$org_id;
    $sales ='sales'.$org_id;
    $avg ='avg'.$org_id;
$reserve ='reserve'.$org_id;
$overage_items = 'overage_items'.$org_id;
$overage_rub = 'overage_rub'.$org_id;

@endphp
  @foreach ($products->where($stock,'>',0) as $product)
  <tr>
        <td class="title_width">{{ $product->title }}</td>
      <td class="title_width">{{ @$my_1c_titles->get($product->id)->title }}</td>
      <td><nobr>{{ round($product->$price) }}&#8202;<small class="text-verymuted">&#8381;</small></nobr></td>
      <td>{{ $product->$stock }}</td>
      <td> {{ $product->$sales }}</td>
      <td> {{ round($product->$avg(),1) }}</td>
      <td> {{ round($product->$reserve(),1) }}</td>
      <td> {{ round($product->$overage_items,1) }}</td>
      <td class="overage_rub"> {{ number_format(round($product->$overage_rub,-2),0,'','&thinsp;') }}</td>

  </tr>
@endforeach
</table>
                    <br><br><br>
<h3>Нулевые остатки</h3>
<table id="zerostock_table" class="table table-hover table-striped table-condensed">
    <thead>
    <tr>
        {{-- <th>Категория</th> --}}
        <th>Общее название</th>
        <th>1С <small>{{$org_title}}</small></th>
        <th>***</th>
        <th>Склад,<br>шт</th>
        <th>Продажи <br>за 3мес</th>
        {{--<th>Средн <br>за месяц</th>--}}
        {{--<th>Запас <br>на 2 мес</th>--}}
        {{--<th>Избыток,<br> шт</th>--}}
        <th  data-class-name="overage_rub">Недостаток,<br> руб</th>
    </tr>
    </thead>
    @foreach ($products->where($stock,'=',0) as $product)

        <tr>
            <td class="title_width">{{ $product->title }}</td>
            <td class="title_width">{{ @$my_1c_titles->get($product->id)->title }}</td>
            <td>{{ round($product->$price) }}&#8202;<small class="text-verymuted">&#8381;</small></td>
            <td>{{ $product->$stock }}</td>
            <td> {{ $product->$sales }}</td>
            {{--<td> {{ round($product->avg,1) }}</td>--}}
            {{--<td> {{ round($product->reserve,1) }}</td>--}}
            {{--<td> {{ round($product->overage_items,1) }}</td>--}}
            <td class="overage_rub"> {{ number_format(round($product->$overage_rub,-2),0,'','&thinsp;') }}</td>

        </tr>
    @endforeach
</table>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
