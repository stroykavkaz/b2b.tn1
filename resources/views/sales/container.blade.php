
@extends('layouts.app')

@section('content')
<div class="container">

 

  <div class="row">
    <div class="col-md-8 col-md-offset-2">

      @include('_partials.errors')
      @include('_partials.flash_msg')

      <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Импорт данных по приходу товара</h3>
Приход контейнера. файл в формате Excel
        </div>
        <div class="panel-body">
          {!! Form::open(array('url' => '/admin/container','method'=>'POST', 'files'=>true)) !!}
          <div class="form-group">
            <strong>Приход контейнера</strong>
            {!! Form::file('container',['required']) !!}
            </div>

          {!! Form::submit('Загрузить', ['class' => 'btn btn-primary']) !!}


          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
