

@extends('layouts.app')
@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-12">

          <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title">Панель директора</h3>

                </div>

                <div class="panel-body">

<table id="sales_table" class="table table-hover table-striped table-condensed">
<thead>
  <tr>
    <th>Филиал</th>
    <th>Себестоимость</th>
    <th>Избыток</th>
    <th>Недостаток</th>
<th>Обновление продаж</th>
      <th>Обновление остатков</th>
  </tr>
</thead>

    <tr>
        <td><a href="/sales/1">Ростов-на-Дону</></td>
        <td>{{number_format(round($total1,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage1_pos,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage1_neg,-3),0,'','&thinsp;')}}</td>

        <td>{{$update_sales1}}</td>
        <td>{{$update_stock1}}</td>
    </tr>
    <tr>
        <td><a href="/sales/2">Санкт-Петербург</></td>
        <td>{{number_format(round($total2,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage2_pos,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage2_neg,-3),0,'','&thinsp;')}}</td>
        <td>{{$update_sales2}}</td>
        <td>{{$update_stock2}}</td>
    </tr>
    <tr>
        <td><a href="/sales/3">Москва</></td>
        <td>{{number_format(round($total3,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage3_pos,-3),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage3_neg,-3),0,'','&thinsp;')}}</td>
        <td>{{$update_sales3}}</td>
        <td>{{$update_stock3}}</td>
    </tr>
    <tr>
        <td><b>Итого</b></td>
        <td>{{number_format(round($total1+$total2+$total3,-4),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage1_pos+$overage2_pos+$overage3_pos,-4),0,'','&thinsp;')}}</td>
        <td>{{number_format(round($overage1_neg+$overage2_neg+$overage3_neg,-4),0,'','&thinsp;')}}</td>
     <td></td>
        <td></td>
    </tr>

</table>   </div>


            </div>


            <div>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Лог логинов</a></li>
                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Лог заливок</a></li>
              </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="home">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <td>Пользователь</td>

                                <td>Последний логин</td>
                                <td>IP</td>
                            </tr></thead>
                            @foreach ($user_logins as $log)

                                <tr>
                                    <td>{{ $log->user['name'] }} </td>

                                    <td>{{$log->created_at->diffForHumans()}}</td>
                                    <td>
                                        <small class="infolabel" title="{{$log->useragent}}">{{ $log->ip_address }}</small>
                                    </td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                    <div role="tabpanel" class="tab-pane" id="profile">
                        <table class="table table-condensed">
                            @foreach ($upload_logs as $log)

                                <tr>
                                    <td>{{$log->created_at->diffForHumans()}}</td>
                                    <td>{{ $log->user['name'] }}</td>
                                    <td>{{$log->action_label}} <a href="/upload/{{$log->attr1}}">{!! $log->object_label !!}</a></td>
                                    <td><small class="infolabel" title="{{$log->useragent}}">{{ $log->ip_address }}</small>
                                    </td>
                                </tr>
                            @endforeach
                        </table> </div>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
