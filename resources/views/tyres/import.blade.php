
@extends('layouts.app')

@section('content')
<div class="container">

  @include('_partials.errors')

  <div class="row">
    <div class="col-md-8 col-md-offset-2">

      @if(Session::has('flash_message'))
          <div class="alert alert-success">
              {{ Session::get('flash_message') }}
          </div>
      @endif

      <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Импорт прайс-листа</h3></div>
        <div class="panel-body">
          {!! Form::open(array('url' => '/tyres/import','method'=>'POST', 'files'=>true)) !!}
          <div class="form-group">
            <strong>Файл</strong>
            {!! Form::file('plTyres') !!}
          </div>
          {!! Form::submit('Загрузить', ['class' => 'btn btn-primary']) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
