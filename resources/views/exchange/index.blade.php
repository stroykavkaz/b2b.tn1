@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <h3>Обмен между филиалами</h3>
По каждому филиалу указан избыток (недостаток) и возможность обмена. <br><br>
                <div class="panel panel-default">
             <table class="table table-hover table-condensed">
                 @if(count($rnd_spb))
                     <tr>
                            <td></td>
                       <td class="exchange_city">Ростов-на-Дону</td>
                         <td></td>
                        <td class="exchange_city">Санкт-Петербург</td>
                        </tr>

                        @foreach ($rnd_spb as $item)
                            <tr>
                             <td class="title_width-x">{{$item->title}}</td>
                             <td class="exchange_city">{{$item->overageItems1}}</td>
                                <td class="exchange_arrow">&#8680;</td>

                             <td class="exchange_city">{{$item->overageItems2}}</td>
                            </tr>
                        @endforeach
                 @endif


                 @if(count($spb_rnd))

                     <tr><td></td>
                        <td class="exchange_city">Ростов-на-Дону</td>
                         <td></td>
                         <td class="exchange_city">Санкт-Петербург</td>
                        </tr>

                    @foreach ($spb_rnd as $item)
                            <tr>
                                <td class="title_width-x">{{$item->title}}</td>
                                <td class="exchange_city">{{$item->overageItems1}}</td>
                                <td class="exchange_arrow">&#8678;</td>
                                <td class="exchange_city">{{$item->overageItems2}}</td>
                            </tr>
                       @endforeach
                      @endif
                    </table>
                </div>


                    <div class="panel panel-default">
                        {{--<div class="panel-heading"><h2 class="panel-title">Обмен между филиалами</h2></div>--}}

                        <table class="table table-hover table-condensed">
                            @if(count($rnd_msk)) <tr>
                                <td></td>
                                <td class="exchange_city">Ростов-на-Дону</td>
                                <td></td>
                                <td class="exchange_city">Москва</td>
                            </tr>

                            @foreach ($rnd_msk as $item)
                                <tr>
                                    <td class="title_width-x">{{$item->title}}</td>
                                    <td class="exchange_city">{{$item->overageItems1}}</td>
                                    <td class="exchange_arrow">&#8680;</td>
                                   <td class="exchange_city">{{$item->overageItems3}}</td>
                                </tr>
                            @endforeach
                            @endif


                            @if(count($msk_rnd))
                            <tr>  <td></td>
                                <td class="exchange_city">Ростов-на-Дону</td>
                             <td></td>
                                <td class="exchange_city">Москва</td>
                            </tr>

                            @foreach ($msk_rnd as $item)
                                <tr>
                                    <td class="title_width-x">{{$item->title}}</td>
                                    <td class="exchange_city">{{$item->overageItems1}}</td>
                                    <td class="exchange_arrow">&#8678;</td>
                                    <td class="exchange_city">{{$item->overageItems3}}</td>
                                </tr>
                            @endforeach
                            @endif
                        </table>



                    </div>


                    <div class="panel panel-default">
                        {{--<div class="panel-heading"><h2 class="panel-title">Обмен между филиалами</h2></div>--}}

                        <table class="table table-hover table-condensed">
                            @if(count($spb_msk))

                            <tr>
                                <td></td>
                                <td class="exchange_city">Санкт-Петербург</td>
                                <td></td>
                                <td class="exchange_city">Москва</td>
                            </tr>

                            @foreach ($spb_msk as $item)
                                <tr>
                                    <td class="title_width-x">{{$item->title}}</td>
                                    <td class="exchange_city">{{$item->overageItems2}}</td>
                                    <td class="exchange_arrow">&#8680;</td>
                                    <td class="exchange_city">{{$item->overageItems3}}</td>
                                </tr>
                            @endforeach
                            @endif


                                @if(count($msk_spb))
                            <tr>  <td></td>
                                <td class="exchange_city">Санкт-Петербург</td>
                                <td></td>
                                <td class="exchange_city">Москва</td>
                            </tr>

                            @foreach ($msk_spb as $item)
                                <tr>
                                    <td class="title_width-x">{{$item->title}}</td>
                                    <td class="exchange_city">{{$item->overageItems2}}</td>
                                    <td class="exchange_arrow">&#8678;</td>
                                    <td class="exchange_city">{{$item->overageItems3}}</td>
                                </tr>
                            @endforeach
                                @endif
                        </table>



                    </div>

</div>
</div>
</div>
@endsection
