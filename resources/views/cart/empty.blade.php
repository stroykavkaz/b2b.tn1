@extends('layouts.app')
@section('content')



<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif


<div class="panel panel-default">
<div class="panel-heading"><h3 class="panel-title">Корзина пока пуста :-)</h3></div>
  <div class="panel-body">
Добавьте позиции, и в течение 30 минут менеджер подтвердит вашу заявку и зарезервирует товар. <br>
Ваш менеджер - Алексей, тел 8-918-554-24-35, alex@tn1.ru
  </div>


</div>
    </div>
  </div>
</div>
@endsection
