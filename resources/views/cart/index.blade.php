@extends('layouts.app')
@section('content')



<div class="container" id="vueapp">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif


              <div class="panel panel-default">


{{-- если корзина не пустая --}}
{{-- @if($items) --}}
  <div class="panel-heading"><h3 class="panel-title">Корзина</h3></div>
  {!! Form::open(array('route' => 'cartconfirm','method'=>'PUT')) !!}  <div class="panel-body">
Проверьте количество и отправьте заявку. <br>
В течение 30 минут менеджер подтвердит и зарезервирует все позиции. <br>
Ваш менеджер - Алексей, тел 8-918-554-24-35, alex@tn1.ru


<vuecart v-bind:cartitems='{!! json_encode($items)  !!}'></vuecart>

</div>
<div class="panel-footer">
  {{-- <input class="btn btn-primary" type="submit" name="submit2" value="Обновить корзину"> --}}

  <input class="btn" type="submit" name="reset" value="Очистить корзину">
  <input class="btn btn-success pull-right" type="submit" name="confirm" value="Отправить заявку">



</div>
  {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
