@if(Session::has('flash_message'))
<div class="alert alert-success">
  {{ Session::get('flash_message') }}
</div>
@endif

@if(Session::has('flash_message1'))
<div class="alert alert-success">
  {{ Session::get('flash_message1') }}
</div>
@endif

@if(Session::has('flash_message2'))
<div class="alert alert-success">
  {{ Session::get('flash_message2') }}
</div>
@endif

@if(Session::has('flash_message3'))
<div class="alert alert-success">
  {{ Session::get('flash_message3') }}
</div>
@endif

@if(Session::has('flash_message4'))
<div class="alert alert-success">
  {{ Session::get('flash_message4') }}
</div>
@endif

@if(Session::has('flash_message5'))
<div class="alert alert-success">
  {{ Session::get('flash_message5') }}
</div>
@endif
