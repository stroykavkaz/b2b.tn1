@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Справочник соответствий</h3>
                        В каждой базе 1С наименования введены под разным именем,
                        поэтому необходимо проставить соответствия.

              <ul>   <li>Если соответствия вашим позициям нет, то выбирайте <samp>"создать новую позицию"</samp> в начале выпадающего списка.
                </li>
                  <li>Первоначально список стандартных названий взят из 1С СтройКомплект-Кавказа,
                      в дальнейшем он будет пополнятся и уточнятся.</li></ul>
                    </div>


                    <div class="panel-body">

                        {{-- <a href="/users/create" class="btn btn-success btn-sm ">Новый пользователь</a> --}}
                    </div>
                    {{Form::open(['method'=>'POST'])}}
                    <table class="table table-hover table-striped table-condensed">
<thead><tr>
    <th>Название в 1С {{$org->name}}</th>
    <th>Стандартные названия для всех филиалов</th></tr></thead>

                        <?php
                            $perPage = 30;
                       $pages =  ceil($titles->count()/$perPage) +1;

                        ?>


                        @for ($i = 0; $i < $pages; $i++)
                            <a href="?{{$i}}">&nbsp;{{$i}}&nbsp;</a> &nbsp;
                        @endfor

                        @foreach ($titles->forPage($page, $perPage) as $t)
        {{--@if(!$t->product_id)--}}
                                <tr>
               <td>{{$t->title}} </td>
               <td>
                   <select name="parent_for[{{$t->id}}]" id="">
                       <option value="">** не использовать (или выберите соответствие)</option>
                       <option value="NEW">** создать новую позицию</option>
                       @foreach($canonical as $c)
                           <option value="{{$c->id}}"
                                   @if($t->product_id==$c->id) selected @endif>{{$c->title}}</option>


                       @endforeach

                   </select>
               </td>

           </tr>
                            {{--@endif--}}
       @endforeach
   </table>
   <div class="panel-footer">

       <button type="submit" class="btn btn-primary">Отправить</button>
   </div>

   {{Form::close()}}

</div>
</div>
</div>
</div>
@endsection
