@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Правильные названия</h3>

                    </div>


                    <div class="panel-body">

                        {{-- <a href="/users/create" class="btn btn-success btn-sm ">Новый пользователь</a> --}}
                    </div>
                    {{Form::open(['method'=>'POST'])}}
                    <table id="sales_table" class="table table-hover table-striped table-condensed">
                        <tr>
                            <th style="width: 50%">1С название</th>
                            <th>Название для b2b</th>
                        </tr>
                        @foreach ($products as $t)
                            <tr>
                                <td>{{$t->title_key}} </td>
                                <td>
                                    <input name="ref[{{$t->id}}]" type="text" value="{{$t->title}}" style="width: 100%;">

                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="panel-footer">

                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>

                    {{Form::close()}}

                </div>
            </div>
        </div>
    </div>
@endsection
