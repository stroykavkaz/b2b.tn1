
@extends('layouts.app')

@section('content')
<div class="container">

  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Ошибка!</strong><br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Редактировать пользователя</h3></div>

                <div class="panel-body">

{{-- {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!} --}}

{!! Form::model($user, [
    'method' => 'PATCH',
    'route' => ['users.update', $user->id]
]) !!}


<div class="form-group">
  <strong>Имя и фамилия</strong>
  {!! Form::text('name', null, array('placeholder' => 'Имя и фамилия','class' => 'form-control')) !!}
 </div>


<div class="form-group">
   <strong>Организация</strong>
   {{ Form::select('org_id', $orgs,$user->org_id,['class' => 'form-control']) }}
</div>

<div class="form-group">
   <strong>Наценка</strong>
   {{ Form::select('margin_id', $margins, $user->margin_id,['class' => 'form-control']) }}
</div>

<div class="form-group">
  <strong>E-mail</strong>
   {!! Form::text('email', null, array('placeholder' => 'E-mail','class' => 'form-control')) !!}
</div>
<div class="form-group">
  <label class="radio-inline">
            <input type="radio" name="is_disabled" id="radio_enabled" value="0" @if(!$user->disabled_at) checked @endif>Активен
          </label>
          <label class="radio-inline">
            <input type="radio" name="is_disabled" id="radio_disabled" value="1" @if($user->disabled_at) checked @endif>Отключен
          </label>
</div>

{{-- <div class="form-group">
    <strong>Пароль</strong>
    {!! Form::text('password', null, array('placeholder' => 'Пароль','class' => 'form-control')) !!}
</div> --}}


<div class="row">


  <div class="col-md-6 text-left">
{!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
</div>


  <div class="col-md-6 text-right">
         {!! Form::open([
             'method' => 'DELETE',
             'route' => ['users.destroy', $user->id],

         ]) !!}

    <input type="submit" value="Удалить" class="btn btn-danger" onclick="return confirm('Удалить пользователя &laquo;{{ $user->name }}&raquo;?')">

         {!! Form::close() !!}
     </div>
</div>

</div>



  </div>
            </div>
        </div>
    </div>
</div>


@endsection
