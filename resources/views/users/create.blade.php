
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Новый пользователь</h3></div>

                <div class="panel-body">

{!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
<div class="form-group">
  <strong>Имя и фамилия</strong>
  {!! Form::text('name', null, array('placeholder' => 'Имя и фамилия','class' => 'form-control','required' => 'required')) !!}
 </div>

 <div class="form-group">
    <strong>Организация</strong>
    {{ Form::select('org_id', $orgs,null,['class' => 'form-control']) }}
 </div>
<div class="form-group">
   <strong>Наценка</strong>
   {{ Form::select('margin_id', $margins,null,['class' => 'form-control']) }}
</div>
<div class="form-group">
  <strong>E-mail</strong>
   {!! Form::email('email', null, array('placeholder' => 'E-mail','class' => 'form-control','required' => 'required')) !!}
</div>




<div class="form-group">
    <strong>Пароль</strong>
    {!! Form::text('password', null, array('placeholder' => 'Пароль','class' => 'form-control','required' => 'required')) !!}
</div>

{!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
