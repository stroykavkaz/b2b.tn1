
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

          @if(Session::has('flash_message'))
              <div class="alert alert-success">
                  {{ Session::get('flash_message') }}
              </div>
          @endif


            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Пользователи</h3></div>


                <div class="panel-body">
<a href="/users/create" class="btn btn-success btn-sm ">Новый пользователь</a>
                </div>

<table class="table table-hover">

{{--    {!! dd($users->toArray()) !!}--}}

  @foreach ($users as $user)
  <tr>
      <td>
  <a href="{{ route('users.edit', $user->id) }}">
    @if($user->disabled_at)
  <s>{{ $user->name }}</s>
@else
    <b>{{ $user->name }}</b>
@endif
</a>
<br>

<small>{{ $user->org->name }}</small><br>
        {{-- <small>{{ $user->email }}</small> --}}
      </td>
<td style="vertical-align: middle;">@if ($user->margin)
{{ $user->margin->title }}<br>
<small>        <a href="/admin/login_as/{{$user->id}}">Войти как {{$user->name}}</a>
</small>  @endif</td>

      <td>
<!-- админа не удаляем и не редактируем -->
        @if ($user->id <> 1)
            <div class="btn-group btn-group-sm pull-right" role="group">
      {{-- Редактировать --}}
        {{-- <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-danger">Удалить</a> --}}
          </div>
        @endif
      </td>
  </tr>


@endforeach


</table>




            </div>




        </div>
    </div>
</div>
@endsection
