
@extends('layouts.app')

@section('content')
<div class="container">

  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Ошибка!</strong><br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Редактировать организацию</h3></div>

                <div class="panel-body">


{!! Form::model($org, [
    'method' => 'PATCH',
    'route' => ['orgs.update', $org->id]
]) !!}



<div class="form-group">
  <strong>Имя и фамилия</strong>
  {!! Form::text('name', null, array('placeholder' => 'Имя и фамилия','class' => 'form-control')) !!}
 </div>

  <div class="form-group">
    <strong>Телефон</strong>
     {!! Form::text('phone', null, array('placeholder' => 'Телефон','class' => 'form-control')) !!}
  </div>

 <div class="form-group">
   <strong>E-mail</strong>
    {!! Form::text('email', null, array('placeholder' => 'E-mail','class' => 'form-control')) !!}
 </div>

 <div class="form-group">
   <strong>Адрес</strong>
    {!! Form::text('address', null, array('placeholder' => 'Адрес','class' => 'form-control')) !!}
 </div>


<div class="row">

  <div class="col-md-6 text-left">
{!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
</div>


  <div class="col-md-6 text-right">
         {!! Form::open([
             'method' => 'DELETE',
             'route' => ['orgs.destroy', $org->id],

         ]) !!}

    <input type="submit" value="Удалить" class="btn btn-danger" onclick="return confirm('Удалить организацию &laquo;{{ $org->name }}&raquo;?')">

         {!! Form::close() !!}
     </div>


</div>



  </div>
            </div>
        </div>
    </div>
</div>


@endsection
