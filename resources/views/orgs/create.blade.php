
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Новая организация</h3></div>

                <div class="panel-body">

{!! Form::open(array('route' => 'orgs.store','method'=>'POST')) !!}
<div class="form-group">
  <strong>Название</strong>
  {!! Form::text('name', null, array('placeholder' => 'Название','class' => 'form-control','required' => 'required')) !!}
 </div>

<div class="form-group">
  <strong>E-mail</strong>
   {!! Form::email('email', null, array('placeholder' => 'E-mail','class' => 'form-control','required' => 'required')) !!}
</div>

<div class="form-group">
  <strong>Телефон</strong>
   {!! Form::text('phone', null, array('placeholder' => 'Телефон','class' => 'form-control','required' => 'required')) !!}
</div>


<div class="form-group">
    <strong>Адрес</strong>
    {!! Form::text('address', null, array('placeholder' => 'Адрес','class' => 'form-control','required' => 'required')) !!}
</div>

{!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
