@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="panel-title">Организации</h3></div>


                    <div class="panel-body">
                        <a href="/orgs/create" class="btn btn-success btn-sm">Новая организация</a>
                    </div>

                    <table class="table table-hover">

                        @foreach ($orgs as $org)
                            <tr>
                                <td>

                                    <a href="{{ route('orgs.edit', $org->id) }}"> <b>{{ $org->name }}</b></a>

                                    <br>
                                    {{ $org->address }}<br>
                                    {{ $org->phone }}<br>

                                </td>
                                <td>
                                    @if($org->id==1)
                                        {{--<a href="/admin/ref">Правильные названия</a>--}}
                                    @else
                                        <a href="/titles/{{$org->id}}">Справочник соответствий</a>

                                    @endif
                                    {{-- <div class="btn-group btn-group-sm pull-right" role="group">
                                <a href="{{ route('orgs.edit', $org->id) }}" class="btn btn-primary">Редактировать</a>

                                  </div> --}}

                                </td>
                            </tr>

                        @endforeach


                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
