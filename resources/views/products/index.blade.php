@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

    

            <div class="panel panel-default">

<table class="table table-hover">
@php
$current_category = ''
@endphp

    <td> </td>
    <td style="width:7em" class="text-verymuted"><small>Розница</small></td>
    <td style="width:7em">Ваша цена</td>

  @foreach ($products as $product)
{{--    @if (($product->category->category <> $current_category) AND $product->title)--}}
      {{--<tr class="active">--}}
        {{--<td>--}}
{{--            <b>{{ $product->category->category }}</b>--}}
        {{--</td>--}}
        {{--<td style="width:7em" class="text-verymuted"><small>Розница</small></td>--}}
        {{--<td style="width:7em">Ваша цена</td>--}}

      {{--</tr>--}}
      @php
{{--    $current_category = $product->category->category--}}
      @endphp

    {{--@endif--}}
  <tr>
      <td>
    {{ $product->title }}
      </td>
      <td class="text-verymuted"><s><small>{{ $product->prices[4] }}&thinsp;&#8381;</small></s></td>
      <td class="">



    <button title="Добавить в корзину" style="width:6em" class="btn btn-primary btn-sm" type="submit" onclick="cartAdd({{$product->id}})">{{ $product->myprice }}&thinsp;&#8381;</button>

      </td>
  </tr>
@endforeach

</table>
            </div>
        </div>
    </div>
</div>
@endsection

<script type="text/javascript">

function cartAdd(product_id) {
  axios.post('/cart', {
    product_id: product_id
  })
  .then(function (response) {
    console.log(response);
     $.notify({
	// options
	 icon: 'glyphicon glyphicon-ok',
	message: '<b>'+response.data.product_title+'</b> успешно добавлен в корзину!'},{	type: 'success'});
  $('#cart_count').text(response.data.cart_count);
  $("#cart_count").highlight('#2b61ff',700);
  })
  .catch(function (error) {
    console.log(error);
  });
}

</script>
