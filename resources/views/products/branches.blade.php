@extends('layouts.app')

@section('content')

    <div class="container my-wide-container">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif


                <div class="panel panel-default">
                    <div class="sticky">
                        <table class="branch_table branch_table_header">
                          <thead>
                          <tr>
                              <td class="title_width"> </td>

                              <td class="vertical" title="Остатки"><b>Остатки</b></td>
                              <td class="vertical" title="Усредненные продажи, 1 мес">Продажи</td>
                              <td class="vertical" title="Избыток"><b>Избыток</b></td>
                              <td class="vertical" title="Поступление">Поступлен</td>
                              <td class="vertical" title="Планируемый остаток после поступления товара">План&nbsp;Остат
                              </td>
                              <td class="vertical" title="Предполагаемый избыток после поступления товара">
                                  План&nbsp;Избыт
                              </td>

                              <td class="vertical" title="Остаток"><b>Остатки</b></td>
                              <td class="vertical" title="Усредненные продажи, 1 мес">Продажи</td>
                              <td class="vertical" title="Избыток"><b>Избыток</b></td>
                              <td class="vertical" title="Поступление">Поступлен</td>
                              <td class="vertical" title="Планируемый остаток после поступления товара">План&nbsp;Остат
                              </td>
                              <td class="vertical" title="Предполагаемый избыток после поступления товара">
                                  План&nbsp;Избыт
                              </td>

                              <td class="vertical" title="Остаток"><b>Остатки</b></td>
                              <td class="vertical" title="Усредненные продажи, 1 мес">Продажи</td>
                              <td class="vertical" title="Избыток"><b>Избыток</b></td>
                              <td class="vertical" title="Поступление">Поступлен</td>
                              <td class="vertical" title="Планируемый остаток после поступления товара">План&nbsp;Остат
                              </td>
                              <td class="vertical" title="Предполагаемый избыток после поступления товара">
                                  План&nbsp;Избыт
                              </td>


                              <td class="vertical" title="Остаток"><b>Остатки</b></td>
                              <td class="vertical" title="Усредненные продажи, 1 мес">Продажи</td>
                              <td class="vertical" title="Избыток"><b>Избыток</b></td>
                              <td class="vertical" title="Поступление">Поступлен</td>
                              <td class="vertical" title="Планируемый остаток после поступления товара">План&nbsp;Остат
                              </td>
                              <td class="vertical" title="Предполагаемый избыток после поступления товара">
                                  План&nbsp;Избыт
                              </td>

                          </tr>
                          <tr>
                              <td>

                              </td>
                              <td class="branch_city" colspan="6"><a href="/sales/1">Ростов-на-Дону</a> <small class="text-verymuted">{{$rnd_updated}}</small></td>
                              <td class="branch_city" colspan="6"><a href="/sales/2">Санкт-Петербург</a> <small class="text-verymuted">{{$spb_updated}}</small></td>
                              <td class="branch_city" colspan="6"><a href="/sales/3">Москва</a> <small class="text-verymuted">{{$msk_updated}}</small></td>
                              <td class="branch_city" colspan="6">Все филиалы</td>
                          </tr>
                          </thead>
                      </table>
                    </div>
                    <table class="branch_table branch_table_data">

                        @foreach ($groups as $group)
                            @php
                            $overage1 = $group->products()->get()->sum(function ($p) {return $p->overageItems1;});
                            $overage2 = $group->products()->get()->sum(function ($p) {return $p->overageItems2;});
                            $overage3 = $group->products()->get()->sum(function ($p) {return $p->overageItems3;});

                            if(App\Http\Controllers\ProductController::canTransfer($overage1,$overage2,$overage3)) $canTransferClass='exchange'; else $canTransferClass='';
                            @endphp

                            <tr class="js-group group_row {{$canTransferClass}}" data-id="{{$group->id}}">
                                <td class="title_width group_title right_border">
                                  {{ $group->title }}
                                </td>

                                <td>{{ $g1stock = $group->products()->get()->sum('stock1') }}</td>
                                <td>{{ $avg1 = $group->products()->get()->sum(function ($p) {return $p->avg1();}) }}</td>
                                <td class="overage">{{ $overage1 }}</td>
                                <td>{{ $cont1 = $group->products()->get()->sum('cont1_1') }}</td>
                                <td>{{ $g1plan = $overage1 + $cont1 }}</td>
                                <td class="right_border">{{ $g1future = $overage1 + $cont1 - $avg1 }}</td>

                                <td>{{ $g2stock =$group->products()->get()->sum('stock2') }}</td>
                                <td>{{ $avg2 = $group->products()->get()->sum(function ($p) {return $p->avg2();}) }}</td>
                                <td class="overage">{{ $overage2 }}</td>
                                <td>{{ $cont2 = $group->products()->get()->sum('cont1_2') }}</td>
                                <td>{{ $g2plan = $overage2 + $cont2 }}</td>
                                <td class="right_border">{{  $g2future =$overage2 + $cont2 - $avg2 }}</td>

                                <td>{{ $g3stock =$group->products()->sum('stock3') }}</td>
                                <td>{{ $avg3 = $group->products()->get()->sum(function ($p) {return $p->avg3();}) }}</td>
                                <td class="overage">{{ $overage3 }}</td>
                                <td>{{ $cont3 = $group->products()->get()->sum('cont1_3') }}</td>
                                <td>{{ $g3plan = $overage3 + $cont3 }}</td>
                                <td>{{  $g3future =$overage3 + $cont3 - $avg3 }}</td>

                                <td>{{ $g1stock+$g2stock+$g3stock }}</td>
                                <td>{{ $avg1+$avg2+$avg3 }}</td>
                                <td class="overage">{{ $overage1+$overage2+$overage3 }}</td>
                                <td>{{ $cont1 + $cont2 + $cont3 }}</td>
                                <td>{{ $g1plan + $g2plan + $g3plan }}</td>
                                <td>{{ $g1future + $g2future + $g3future }}</td>

                            </tr>


                            @foreach ($group->products()->get() as $gproduct)
                                {{-- пустые позиции не выводим (нет остатков, нет продаж)--}}
                                @if($gproduct->stock1 OR $gproduct->stock2 OR $gproduct->stock3 OR $gproduct->sales1 OR $gproduct->sales2 OR $gproduct->sales3)

                                <tr class="gproducts gproducts_{{$group->id}}
                                    @if(App\Http\Controllers\ProductController::canTransfer($gproduct->overageItems1,$gproduct->overageItems2,$gproduct->overageItems3)) exchange @endif">
                                        <td class="title_width">
                                           {{ $gproduct->title }}

                                        </td>

                                        <td>{{ $gproduct->stock1 }}</td>
                                        <td>{{ $gproduct->avg1() }}</td>
                                        <td class="overage">{{ $gproduct->overageItems1 }}</td>
                                        <td>{{ $gproduct->cont1_1 }}</td>
                                        <td>{{ $gi1plan = $gproduct->overageItems1+$gproduct->cont1_1 }}</td>
                                        <td class="right_border">{{ $gi1future = round(($gproduct->overageItems1+$gproduct->cont1_1)-$gproduct->avg1()) }}</td>


                                        <td>{{ $gproduct->stock2 }}</td>
                                        <td>{{ $gproduct->avg2() }}</td>
                                        <td class="overage">{{ $gproduct->overageItems2 }}</td>
                                        <td>{{ $gproduct->cont1_2 }}</td>
                                        <td>{{ $gi2plan = $gproduct->overageItems2+$gproduct->cont1_2 }}</td>
                                        <td class="right_border">{{ $gi2future = round(($gproduct->overageItems2+$gproduct->cont1_2)-$gproduct->avg2()) }}</td>


                                        <td>{{ $gproduct->stock3 }}</td>
                                        <td>{{ $gproduct->avg3() }}</td>
                                        <td class="overage">{{ $gproduct->overageItems3 }}</td>
                                        <td>{{ $gproduct->cont1_3 }}</td>
                                        <td>{{ $gi3plan = $gproduct->overageItems3+$gproduct->cont1_3 }}</td>
                                        <td>{{ $gi3future = round(($gproduct->overageItems3+$gproduct->cont1_3)-$gproduct->avg3()) }}</td>

                                    <td>{{ $gproduct->stock1+ $gproduct->stock2+ $gproduct->stock3 }}</td>
                                    <td>{{ $gproduct->avg1() +$gproduct->avg2()+ $gproduct->avg3() }}</td>
                                    <td class="overage">{{ $gproduct->overageItems1+ $gproduct->overageItems2+ $gproduct->overageItems3 }}</td>
                                    <td>{{ $gproduct->cont1_1+ $gproduct->cont1_2 +$gproduct->cont1_3 }}</td>
                                    <td>{{ $gi1plan+$gi2plan+$gi3plan }}</td>
                                    <td>{{ $gi1future +$gi2future+$gi3future }}</td>

                                </tr>
                                    @endif
                                    @endforeach
                        @endforeach

                        @foreach ($products as $product)
                            {{-- пустые позиции не выводим (нет остатков, нет продаж)--}}
                            @if ($product->stock1 OR $product->stock2 OR $product->stock3 OR $product->sales1 OR $product->sales2 OR $product->sales3)

                            <tr @if($product->exchange) class="exchange" @endif>
                                <td class="title_width right_border">{{ $product->title }}</td>

                                <td>{{ $product->stock1 }}</td>
                                <td>{{ $product->avg1() }}</td>
                                <td class="overage">{{ $product->overageItems1 }}</td>
                                <td>{{ $product->cont1_1 }}</td>
                                <td>{{ $i1plan = $product->overageItems1+$product->cont1_1 }}</td>
                                <td class="right_border">{!! $i1future = round(($product->overageItems1+$product->cont1_1)-$product->sales1) !!}</td>


                                <td>{{ $product->stock2 }}</td>
                                <td>{{ $product->avg2() }}</td>
                                <td class="overage">{{ $product->overageItems2 }}</td>
                                <td>{{ $product->cont1_2 }}</td>
                                <td>{{ $i2plan = $product->overageItems2+$product->cont1_2 }}</td>
                                <td class="right_border">{!! $i2future = round(($product->overageItems2+$product->cont1_2)-$product->sales2) !!}</td>


                                <td>{{ $product->stock3 }}</td>
                                <td>{{ $product->avg3() }}</td>
                                <td class="overage">{{ $product->overageItems3 }}</td>
                                <td>{{ $product->cont1_3 }}</td>
                                <td>{{ $i3plan = $product->overageItems3+$product->cont1_3 }}</td>
                                <td class="right_border">{{ $i3future = round(($product->overageItems3+$product->cont1_3)-$product->sales3) }}</td>

                                <td>{{ $product->stock1+$product->stock2+$product->stock3  }}</td>
                                <td>{{ $product->avg1() + $product->avg2() + $product->avg3() }}</td>
                                <td class="overage">{{ $product->overageItems1+$product->overageItems2+$product->overageItems3 }}</td>
                                <td>{{ $product->cont1_1 + $product->cont1_2  + $product->cont1_3 }}</td>
                                <td>{{ $i1plan+$i2plan+$i3plan }}</td>
                                <td>{{ $i1future+$i2future+$i3future }}</td>

                            </tr>
                            @endif
                        @endforeach

                    </table>
                   <div class="panel-body">
                       <ul>
                           <li>
                               Позиции, по которым возможен обмен между филиалами, <span class="exchange">выделены цветом</span>.
                           </li>
                           <li>
                               Избыток = Остаток минус (продажи * 2 месяца).

                           </li>
                       </ul>
                   </div>


                    </p>

                </div>
            </div>
        </div>
    </div>
@endsection
