
@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

          @if(Session::has('flash_message'))
              <div class="alert alert-success">
                  {{ Session::get('flash_message') }}
              </div>
          @endif


            <div class="panel panel-default">
                {{-- <div class="panel-heading"><h3 class="panel-title">Доступные позиции</h3></div> --}}


                <div class="panel-body">
{{-- <a href="/orgs/create" class="btn btn-success btn-sm">Новая организация</a> --}}
  Курс {{ $last_usd->rub_rate }}, обновлен  {{ date('d/m H:m', strtotime($last_usd->created_at))  }}
                </div>

<table class="table table-hover">
<th>

</th>
  <th>Опорная</th>

@foreach ($margins as $margin)
      <th>    {{ $margin->title }}   <span style="font-weight:normal">{{ $margin->procent }}</span>   </th>
@endforeach


@foreach ($products as $product)
  <tr>
      <td>

    {{ $product->title }}

      </td>
      <td>
        ${{ $product->price }}

      </td>

      @foreach ($margins as $margin)
            <td>
              {{ $product->prices[$margin->id] }}р.<br>
                <small> ${{ ceil($product->price * $margin->margin) }}</small> <br>
                </td>
      @endforeach

  </tr>

@endforeach


</table>

            </div>
        </div>
    </div>
</div>
@endsection
