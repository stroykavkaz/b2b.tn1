
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            @if(Session::has('flash_message'))
                <div class="alert alert-success">
                    {{ Session::get('flash_message') }}
                </div>
            @endif


              <div class="panel panel-default">
                  <div class="panel-heading"><h3 class="panel-title">Наценки и скидки</h3></div>


        <div class="panel-body">
          <a href="/margins/create" class="btn btn-success btn-sm">Новая наценка</a>
        </div>

        <table class="table table-hover">

          @foreach ($margins as $margin)
            <tr>
              <td>
                <a href="{{ route('margins.edit', $margin->id) }}">
                <b>{{ $margin->title }}</b>
              </a>
                <br>
                   {{-- {{ $margin->margin }} --}}



              </td>
              <td>
      {{  $margin->procent }}
            </td>
              <td>
                <div class="btn-group btn-group-sm pull-right" role="group">

                </div>
              </td>
            </tr>

          @endforeach


        </table>

      </div>
    </div>
  </div>
</div>
@endsection
