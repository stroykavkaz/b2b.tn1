
@extends('layouts.app')

@section('content')
<div class="container">

  @if (count($errors) > 0)
      <div class="alert alert-danger">
          <strong>Ошибка!</strong><br><br>
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Редактировать наценки</h3></div>

                <div class="panel-body">


{!! Form::model($margin, [
    'method' => 'PATCH',
    'route' => ['margins.update', $margin->id]
]) !!}



<div class="form-group">
  <strong>Название</strong>
  {!! Form::text('title', null, array('placeholder' => 'Название','class' => 'form-control')) !!}
 </div>

  <div class="form-group">
    <strong>Коэффициент</strong>
     {!! Form::number('margin', null, array('placeholder' => 'процент','class' => 'form-control','step'=>"0.01",'min'=>'0.5','max'=>'5')) !!}
  </div>

<div class="row">

  <div class="col-md-6 text-left">
{!! Form::submit('Редактировать', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
</div>


  <div class="col-md-6 text-right">
         {!! Form::open([
             'method' => 'DELETE',
             'route' => ['margins.destroy', $margin->id],

         ]) !!}

    <input type="submit" value="Удалить" class="btn btn-danger" onclick="return confirm('Удалить наценку &laquo;{{ $margin->title }}&raquo;?')">

         {!! Form::close() !!}
     </div>


</div>



  </div>
            </div>
        </div>
    </div>
</div>


@endsection
