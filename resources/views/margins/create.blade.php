
@extends('layouts.app')

@section('content')
<div class="container">

@include('_partials.errors')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Новая наценка</h3></div>

                <div class="panel-body">

{!! Form::open(array('route' => 'margins.store','method'=>'POST')) !!}
<div class="form-group">
  <strong>Название</strong>
  {!! Form::text('title', null, array('placeholder' => 'Название','class' => 'form-control','required' => 'required')) !!}
 </div>

<div class="form-group">
    <strong>Процент</strong>
    {!! Form::text('margin', null, array('placeholder' => 'Наценка','class' => 'form-control','required' => 'required','step'=>"0.01",'min'=>'0.5','max'=>'5')) !!}
</div>

{!! Form::submit('Добавить', ['class' => 'btn btn-primary']) !!}
{!! Form::close() !!}
  </div>
            </div>
        </div>
    </div>
</div>
@endsection
